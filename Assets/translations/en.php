<?php
return [
    'langName'     => 'English',
    'emailAddress' => 'Email address',
    'password'     => 'Password',
    'pleaseSingIn' => 'Please sign in',
    'rememberMe'   => 'Remember me',
    'singIn'       => 'Sign in',
    'signOut'      => 'Sign out',
    'next'         => 'next',
    'changeTheme'  => 'Change theme',
];
