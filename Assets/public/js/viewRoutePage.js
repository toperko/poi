var trojmiasto = {lat: 54.499742, lng: 18.539242};

function initMap() {
    $.ajax({
        dataType: "json",
        url: $(location).attr('origin') + '/ajax/route',
        success: function (data) {
            generateMaps(data);
        }
    });
}

function generateMaps(data) {
    $.each(data, function (i, items) {
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();
        var map = new google.maps.Map(document.getElementById('route-map-' + i), {
            zoom: 11,
            center: trojmiasto,
            disableDefaultUI: true, // a way to quickly hide all controls
            scaleControl: true,
            zoomControl: true,
        });
        directionsDisplay.setMap(map);
        var waypts = [];
        $.each(items, function (iPlace, item) {
            if (iPlace == 0 || iPlace == items.length - 1) {
                return true;
            }
            waypts.push({location: {lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)}, stopover: true});
        });
        start = {lat: parseFloat(items[0].latitude), lng: parseFloat(items[0].longitude)};
        end = {lat: parseFloat(items[items.length - 1].latitude), lng: parseFloat(items[items.length - 1].longitude)};
        var request = {
            origin: start,
            destination: end,
            waypoints: waypts,
            optimizeWaypoints: false,
            unitSystem: google.maps.UnitSystem.METRIC,
            travelMode: 'DRIVING'
        };
        directionsService.route(request, function (result, status) {
            if (status == 'OK') {
                directionsDisplay.setDirections(result);
                var totalDistance = 0;
                var totalDuration = 0;
                var legs = result.routes[0].legs;
                for (var iLegs = 0; iLegs < legs.length; ++iLegs) {
                    totalDistance += legs[iLegs].distance.value;
                    totalDuration += legs[iLegs].duration.value;
                }
                let box = $('#route-result-' + i);
                box.attr('distance', totalDistance);
                box.attr('duration', totalDuration);
                $('#distance-' + i).text(totalDistance / 1000);
                $('#duration-' + i).text(moment().startOf('day')
                    .seconds(totalDuration)
                    .format('H [hours] mm [minutes] ss [seconds]'));
            }
        });
    });
}

$(document).ready(function () {
    $("#sort-distance").click(function () {
        $(this).removeClass('btn-light');
        $("#sort-time").removeClass('btn-primary');
        $(this).addClass('btn-primary');
        $("#route-results").html($('.route-result').sort(function (a, b) {
            var contentA = parseInt($(a).attr('distance'));
            var contentB = parseInt($(b).attr('distance'));

            console.log(contentA);
            console.log(contentB);
            return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
        }));
    });

    $("#sort-time").click(function () {
        $("#sort-distance").removeClass('btn-primary');
        $(this).removeClass('btn-light');
        $(this).addClass('btn-primary');
        $("#route-results").html($('.route-result').sort(function (a, b) {
            var contentA = parseInt($(a).attr('duration'));
            var contentB = parseInt($(b).attr('duration'));

            console.log(contentA);
            console.log(contentB);
            return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
        }));
    });
});
