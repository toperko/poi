let map;
let markers = [];
let visibleMarker = null;

function initMap() {
    var trojmiasto = {lat: 54.499742, lng: 18.539242};

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: trojmiasto,
        disableDefaultUI: true, // a way to quickly hide all controls
        scaleControl: true,
        zoomControl: true,
    });
    google.maps.event.addListenerOnce(map, 'idle', function () {
        $.ajax({
            dataType: "json",
            url: $(location).attr('origin') + '/ajax/product/best',
            success: function (data) {
                console.log(data);
                generateHtml(data);
                generateMarkers(data);
            }
        });
    });
}

function generateMarkers(data) {
    $.each(data, function (i, item) {
        generateMarker(item.location);
    });
}

function generateMarker(item) {
    let myLatLng = {lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)};
    let marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: 'http://maps.google.com/mapfiles/ms/icons/' + item.color + '-dot.png',
        id: item.id
    });
    google.maps.event.addListener(marker, 'click', function() {
        clickOnMarker(marker);
    });
    marker.setVisible(false);
    markers[item.id] = marker;
}

function generateHtml(data) {
    let productList = $("#product-list");
    $.each(data, function (i, item) {
        let itemDiv = $('<div>');
        itemDiv.addClass('col-md-12 rounded');
        let itemLinkMap = $('<a>');
        itemLinkMap.addClass('btn btn-primary seeOnMap');
        itemLinkMap.attr('href', '#map');
        itemLinkMap.attr('data', item.location.id);

        let itemLinkDetails = $('<a>');
        itemLinkDetails.addClass('btn btn-secondary');
        itemLinkDetails.attr('href', $(location).attr('origin') + '/place/' + item.location.id);
        itemLinkDetails.attr('data', item.location.id);


        let itemAddRouteLink = $('<a>');
        itemAddRouteLink.addClass('btn btn-success');
        itemAddRouteLink.attr('href', $(location).attr('origin') + '/route/localization/add/' + item.id);

        itemDiv.append(
            $('<h5>').text(item.name),
            $('<p>').text(item.priceString),
            itemLinkMap.text('See on map'),
            itemLinkDetails.text('Details'),
            itemAddRouteLink.text('Add to route'),
        );
        productList.append(itemDiv)
    });

    $("#product-list div a.seeOnMap").click(function (event) {
        if(visibleMarker){
            markers[visibleMarker].setVisible(false);
        }

        markers[$(this).attr('data')].setVisible(true);
        visibleMarker = $(this).attr('data');
    });
}

function clickOnMarker(marker) {
    $( "#places-list div" ).removeClass('show');
    $( "#places-list div[data="+marker.id+"]" ).addClass('show');
}