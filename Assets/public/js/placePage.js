let map;

function initMap() {
    var lat = $("#map").attr('lat');
    var lng = $("#map").attr('lng');
    var point = {lat: parseFloat(lat), lng: parseFloat(lng)};

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: point,
        disableDefaultUI: true, // a way to quickly hide all controls
        scaleControl: true,
        zoomControl: true,
    });

    let marker = new google.maps.Marker({
        position: point,
        map: map
    });
}

