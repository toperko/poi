let map;
let markers = [];

function initMap() {

    let inputLat = $("#mapPreview").attr('lat');
    let inputLng = $("#mapPreview").attr('lng');

    let myLatLng = {lat: parseFloat(inputLat), lng: parseFloat(inputLng)};
    let myOptions = {
        zoom: 14,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("mapPreview"), myOptions);

    let marker = new google.maps.Marker({
        position: myLatLng,
        map: map
    });
    markers.push(marker);

}

function setMapOnAll(map) {
    for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearMarkers() {
    setMapOnAll(null);
}

function deleteMarkers() {
    clearMarkers();
    markers = [];

}

function addMarker(location) {
    deleteMarkers();
    $("#inputPlaceLatitude").val(location.lat());
    $("#inputPlaceLongitude").val(location.lng());
    geocodePosition(location);
    let marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);
}

function geocodePosition(pos) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: pos
    }, function(responses) {
        if (responses && responses.length > 0) {
            $("#inputPlaceAddress").val(responses[0].formatted_address);
        } else {
            $("#inputPlaceAddress").val('Cannot determine address at this location.');
        }
    });
}
