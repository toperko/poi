let map;
let markers = [];

function initMap() {
    var trojmiasto = {lat: 54.499742, lng: 18.539242};
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: trojmiasto,
        disableDefaultUI: true, // a way to quickly hide all controls
        scaleControl: true,
        zoomControl: true,
    });
    google.maps.event.addListenerOnce(map, 'idle', function () {
        $.ajax({
            dataType: "json",
            url: $(location).attr('href') + 'ajax/places',
            success: function (data) {
                generateHtml(data);
                generateMarkers(data);
            }
        });
    });
}

function generateMarkers(data) {
    $.each(data, function (i, item) {
        generateMarker(item);
    });
}

function generateMarker(item) {
    let myLatLng = {lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)};
    let marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: 'http://maps.google.com/mapfiles/ms/icons/' + item.color + '-dot.png',
        id: item.id
    });
    google.maps.event.addListener(marker, 'click', function () {
        clickOnMarker(marker);
    });
    markers.push(marker);
}

function generateHtml(data) {
    let placesList = $("#places-list");
    $.each(data, function (i, item) {
        let itemDiv = $('<div>');
        itemDiv.attr('data', item.id)
        itemDiv.addClass('col-md-12 rounded');
        let itemLink = $('<a>');
        itemLink.addClass('btn btn-primary');
        itemLink.attr('href', $(location).attr('href') + 'place/' + item.id);
        let itemAddRouteLink = $('<a>');
        itemAddRouteLink.addClass('btn btn-success');
        itemAddRouteLink.attr('href', $(location).attr('href') + 'route/localization/add/' + item.id);
        itemDiv.append(
            $('<h3>').text(item.name),
            $('<p>').text(item.description),
            itemLink.text('Check'),
            itemAddRouteLink.text('Add to route')
        );
        itemDiv.addClass('collapse');
        placesList.append(itemDiv)
    });
}

function clickOnMarker(marker) {
    $("#places-list div").removeClass('show');
    $("#places-list div[data=" + marker.id + "]").addClass('show');
}