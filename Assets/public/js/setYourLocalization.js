let map;
let markers = [];

function initMap() {
    var inputLat = $("#inputLatitude").val();
    var inputLng = $("#inputLongitude").val();
    if (inputLat != '' && inputLng != 0) {
        var myLocation = {lat: parseFloat(inputLat), lng: parseFloat(inputLng)};
        var myZoom = 14;
    } else {
        var myLocation = {lat: 54.440314, lng: 18.560298};
        var myZoom = 10;
    }
    let myOptions = {
        zoom: myZoom,
        center: myLocation,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("setYourLocalization"), myOptions);
    map.addListener('click', function (event) {
        addMarker(event.latLng);
    });

    if (inputLat != '' && inputLng != 0) {
        let myLatLng = {lat: parseFloat(inputLat), lng: parseFloat(inputLng)};
        let marker = new google.maps.Marker({
            position: myLatLng,
            map: map
        });
        markers.push(marker);
    }
}

function setMapOnAll(map) {
    for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearMarkers() {
    setMapOnAll(null);
}

function deleteMarkers() {
    clearMarkers();
    markers = [];
}

function addMarker(location) {
    deleteMarkers();
    $("#inputLatitude").val(location.lat());
    $("#inputLongitude").val(location.lng());
    geocodePosition(location);
    let marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);
}

function geocodePosition(pos) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: pos
    }, function (responses) {
        if (responses && responses.length > 0) {
            $("#inputAddress").val(responses[0].formatted_address);
        } else {
            $("#inputAddress").val('Cannot determine address at this location.');
        }
    });
}
