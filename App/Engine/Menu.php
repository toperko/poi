<?php
declare(strict_types=1);
namespace App\Engine;

use App\Engine\Router\Router;
use App\Src\User\UserService;

/**
 * Class Menu
 *
 * @package App\Engine
 */
class Menu
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * Menu constructor.
     *
     * @param Router      $router
     * @param UserService $userService
     */
    public function __construct(Router $router, UserService $userService)
    {
        $this->router = $router;
        $this->userService = $userService;
    }
}
