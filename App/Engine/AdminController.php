<?php
declare(strict_types=1);
namespace App\Engine;

/**
 * Class AdminController
 *
 * @package App\Engine
 */
class AdminController extends Controller
{
    /**
     * @var Cookie
     */
    private $cookie;

    /**
     * AdminController constructor.
     *
     * @param App $app
     *
     * @throws Container\ContainerException
     * @throws Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->cookie = $this->app->getContainer()->get('cookie');
    }

    /**
     * @return View
     * @throws Container\ContainerException
     * @throws Container\ContainerNotFoundException
     * @throws \Throwable
     */
    protected function getView() : View
    {
        $view = parent::getView();
        $view->setBasePath('admin/');
        $view->addCss('admin-style', 'admin/style.css');
        if ($this->cookie->has(View::THEME_COOKIE_KEY) &&
            $this->cookie->get(View::THEME_COOKIE_KEY, FILTER_SANITIZE_STRING) === View::THEME_DARK
        ) {
            $view->addCss('theme-dark', 'admin/styleDark.css');
        }

        return $view;
    }
}
