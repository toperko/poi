<?php
declare(strict_types=1);
namespace App\Engine;

/**
 * Class FrontendController
 *
 * @package App\Engine
 */
class FrontendController extends Controller
{
    /**
     * @return View
     * @throws Container\ContainerException
     * @throws Container\ContainerNotFoundException
     * @throws \Throwable
     */
    protected function getView() : View
    {
        $view = parent::getView();
        $view->setBasePath('frontend/');
        $view->addCss('frontend-style', 'frontend/style.css');

        return $view;
    }
}
