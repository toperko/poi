<?php
declare(strict_types=1);
namespace App\Engine\Router;

use App\Engine\Session\Session;
use App\Src\User\UserConst;
use App\Src\User\UserService;

class Router
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var RouteCollection
     */
    protected static $collection;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $bundle;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Router constructor.
     *
     * @param string               $url
     * @param RouteCollection|null $collection
     * @param UserService|null     $userService
     * @param Session|null         $session
     */
    public function __construct(string $url, ?RouteCollection $collection = null, ?UserService $userService = null, ?Session $session = null)
    {
        if ($collection != null) {
            Router::$collection = $collection;
        }
        $url = explode('?', $url);
        $this->url = $url[0];
        $this->userService = $userService;
        $this->session = $session;
    }

    /**
     * @param array $collection
     */
    public function setCollection($collection)
    {
        Router::$collection = $collection;
    }

    /**
     * @return RouteCollection
     */
    public function getCollection() : RouteCollection
    {
        return Router::$collection;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param String $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return String
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sprawdza czy URL pasuje do przekazanej reguły.
     *
     * @param Route $route Obiekt reguły
     *
     * @return bool
     * @throws \Exception
     */
    protected function matchRoute($route)
    {
        $params = [];
        $key_params = array_keys($route->getParams());
        $value_params = $route->getParams();
        foreach ($key_params as $key) {
            $params['<' . $key . '>'] = $value_params[$key];
        }
        $url = $route->getPath();
        // Zamienia znaczniki na odpowiednie wyrażenia regularne
        $url = str_replace(array_keys($params), $params, $url);
        // Jeżeli brak znacznika w tablicy $params zezwala na dowolny znak
        $url = preg_replace('/<\w+>/', '.*', $url);
        // sprawdza dopasowanie do wzorca
        preg_match("#^$url$#", $this->url, $results);
        if ($results) {
            if ($route->getAuthLevel() > UserConst::ANONYMOUS) {
                if (!$this->userService->checkUserIsLogin()) {
                    $this->session->set('redirect', HTTP_SERVER . $_SERVER["REQUEST_URI"]);
                    $this->redirect($this->generateUrl('login'));
                }
                if ($this->userService->getUserLoginLevel() < $route->getAuthLevel()) {
                    $this->redirect($this->generateUrl('404'));
                }
            }
            $this->url = str_replace([$this->strlcs($url, $this->url)], [''], $this->url);
            $this->class = $route->getClass();
            $this->method = $route->getMethod();
            $this->bundle = $route->getBundle();

            return true;
        }

        return false;
    }

    /**
     * Szuka odpowiedniej reguły pasującej do URL. Jeżeli znajdzie zwraca true.
     *
     * @return bool
     * @throws \Exception
     */
    public function run()
    {
        foreach (Router::$collection->getAll() as $route) {
            if ($this->matchRoute($route)) {
                $this->setGetData($route);

                return true;
            }
        }

        return false;
    }

    /**
     * @param Route $route Obiekt Route pasujący do reguły
     */
    protected function setGetData($route)
    {
        $routePath = str_replace(['(', ')'], ['', ''], $route->getPath());
        $trim = explode('<', $routePath);
        $parsed_url = str_replace([HTTP_SERVER], [''], $this->url);
        $parsed_url = preg_replace("#$trim[0]#", '', $parsed_url, 1);
        // ustawia parametry przekazane w URL
        if (strlen($parsed_url)) {
            foreach ($route->getParams() as $key => $param) {
                if ($parsed_url[0] == '/') {
                    $parsed_url = substr($parsed_url, 1);
                }
                preg_match("#$param#", $parsed_url, $results);
                if (!empty($results[0])) {
                    $_GET[$key] = $results[1];
                    $temp_url = explode($results[0], $parsed_url, 2);
                    $parsed_url = $temp_url[1];
                }
            }
        }
        // jezeli brak parametru w URL ustawia go z tablicy wartości domyślnych
        foreach ($route->getDefaults() as $key => $default) {
            if (!isset($_GET[$key])) {
                $_GET[$key] = $default;
            }
        }
    }

    /**
     * Zwraca część wspólną ciągów
     *
     *
     *
     * @param string $str1 Ciąg 1
     * @param string $str2 Ciąg 2
     *
     * @return string część wspólna
     */
    protected function strlcs($str1, $str2)
    {
        $str1Len = strlen($str1);
        $str2Len = strlen($str2);
        $ret = [];
        if ($str1Len == 0 || $str2Len == 0)
            return ''; //no similarities
        $CSL = []; //Common Sequence Length array
        $intLargestSize = 0;
        //initialize the CSL array to assume there are no similarities
        for ($i = 0; $i < $str1Len; $i++) {
            $CSL[$i] = [];
            for ($j = 0; $j < $str2Len; $j++) {
                $CSL[$i][$j] = 0;
            }
        }
        for ($i = 0; $i < $str1Len; $i++) {
            for ($j = 0; $j < $str2Len; $j++) {
                //check every combination of characters
                if ($str1[$i] == $str2[$j]) {
                    //these are the same in both strings
                    if ($i == 0 || $j == 0)
                        //it's the first character, so it's clearly only 1 character long
                        $CSL[$i][$j] = 1;
                    else
                        //it's one character longer than the string from the previous character
                        $CSL[$i][$j] = $CSL[$i - 1][$j - 1] + 1;
                    if ($CSL[$i][$j] > $intLargestSize) {
                        //remember this as the largest
                        $intLargestSize = $CSL[$i][$j];
                        //wipe any previous results
                        $ret = [];
                        //and then fall through to remember this new value
                    }
                    if ($CSL[$i][$j] == $intLargestSize)
                        //remember the largest string(s)
                        $ret[] = substr($str1, $i - $intLargestSize + 1, $intLargestSize);
                }
                //else, $CSL should be set to 0, which it was already initialized to
            }
        }
        //return the list of matches
        if (isset($ret[0])) {
            return $ret[0];
        } else {
            return '';
        }
    }

    /**
     * @param null|string $name
     * @param array|null  $data
     *
     * @return string
     * @throws \Exception
     */
    public function generateUrl(?string $name, ?array $data = null) : string
    {
        if (!$name && !$data) {
            return HTTP_SERVER . '/';
        }
        $collection = self::$collection;
        $route = $collection->get($name);
        if (isset($route)) {
            return $route->geneRateUrl($data);
        } else {
            throw new \Exception("Nie znaleziono akcji");
        }
    }

    /**
     * @param string $url
     */
    public function redirect(string $url) : void
    {
        flush();
        header("location: " . $url);
        exit();
    }
} 

