<?php
declare(strict_types=1);
namespace App\Engine\Router;

class Route
{
    /**
     * @var string Ścieżka URL
     */
    protected $path;

    /**
     * @var string|null Nazwa pakietu
     */
    protected $bundle;

    /**
     * @var string Nazwa klasy
     */
    protected $class;

    /**
     * @var string Nazwa metody
     */
    protected $method;

    /**
     * @var array Zawiera wartości domyślne dla parametrów
     */
    protected $defaults = [];

    /**
     * @var array Zawiera reguły przetważania dla parametrów
     */
    protected $params = [];

    /**
     * @var integer
     */
    protected $authLevel;

    /**
     * @param string $path     Ścieżka URL
     * @param array  $config   Tablica ze ścieżką do kontrolera oraz nazwą metody
     * @param array  $params   Tablica reguł przetważania dla parametrów
     * @param array  $defaults Tablica wartości domyślne parametrów
     */
    public function __construct(string $path, array $config, array $params = [], array $defaults = [])
    {
        $this->path = $path;
        $this->method = $config['method'];
        if (isset($config['bundle'])) {
            $this->bundle = $config['bundle'];
        }
        $this->class = $config['class'];
        $this->authLevel = 0;
        if (isset($config['authLevel'])) {
            $this->authLevel = $config['authLevel'];
        }
        $this->setParams($params);
        $this->setDefaults($defaults);
    }

    /**
     * @param string $class
     */
    public function setClass(string $class)
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getClass() : string
    {
        return $this->class;
    }

    /**
     * @param array $defaults
     */
    public function setDefaults(array $defaults)
    {
        $this->defaults = $defaults;
    }

    /**
     * @return array
     */
    public function getDefaults() : array
    {
        return $this->defaults;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getMethod() : string
    {
        return $this->method;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams() : array
    {
        return $this->params;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath() : string
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function getAuthLevel() : int
    {
        return $this->authLevel;
    }

    /**
     * @return string|null
     */
    public function getBundle() : ?string
    {
        return $this->bundle;
    }

    /**
     * Generuje przyjazny link.
     *
     * @param array|null $data
     *
     * @return string
     */
    public function generateUrl(?array $data = null) : string
    {
        if ($data) {
            $key_data = array_keys($data);
            $data2 = [];
            foreach ($key_data as $key) {
                $data2['<' . $key . '>'] = $data[$key];
            }
            $url = str_replace(['?', '(', ')'], ['', '', ''], $this->path);

            return str_replace(array_keys($data2), $data2, $url);
        }
        $url = preg_replace("#<[a-zA-Z0-9]*>#", '', $this->path, 1);

        return str_replace(['?', '(', ')'], ['', '', ''], $url);
    }
}
