<?php
namespace App;

/**
 * Class Helper
 *
 * @package App
 */
class Helper
{
    /**
     * @param int $length
     *
     * @return string
     */
    public static function randomHash(int $length = 20) : string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * @param array $array
     *
     * @return array
     */
    public static function permutations(array $array) : array
    {
        $result = [];
        $recurse = function($array, $start_i = 0) use (&$result, &$recurse) {
            if ($start_i === count($array) - 1) {
                array_push($result, $array);
            }
            for ($i = $start_i; $i < count($array); $i++) {
                //Swap array value at $i and $start_i
                $t = $array[$i];
                $array[$i] = $array[$start_i];
                $array[$start_i] = $t;
                //Recurse
                $recurse($array, $start_i + 1);
                //Restore old order
                $t = $array[$i];
                $array[$i] = $array[$start_i];
                $array[$start_i] = $t;
            }
        };
        $recurse($array);

        return $result;
    }
}
