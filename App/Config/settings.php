<?php
return [
    'settings' => [
        'applicationHost'        => 'localhost',
        'applicationPort'        => 8000,
        'applicationName'        => 'Poi',
        'displayErrorDetails'    => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'db'                     => [
            'driver'  => "mysql",
            'host'    => "mysqlPoi",
            'port'    => 3306,
            'user'    => "root",
            'pass'    => "root",
            'dbname'  => "poi",
            'charset' => 'utf8',
        ],
        // Monolog settings
        'logger'                 => [
            'name'  => 'slim-app',
            'path'  => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../Logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'view'                   => [
            'dir' => __DIR__ . '/../View',
        ],
        'googleMapsApiKey' => 'AIzaSyBrTXkQGFdBrpmECCIaEkiyaLCbX94e4v0',
    ],
];
