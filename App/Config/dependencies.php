<?php
$container = $app->getContainer();
$container['db'] = function($container) {
    $settings = $container->get('settings');
    $db = $settings['db'];
    if (!$db) {
        throw new Exception('Brak klucza db w settings.php');
    }
    $pdo = new \PDO(
        $db['driver'] . ':host=' . $db['host'] . ';dbname=' . $db['dbname'] . ';charset=' . $db['charset'] . ';port=' . $db['port'],
        $db['user'],
        $db['pass']
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);

    return $pdo;
};
$container['router'] = function(\Psr\Container\ContainerInterface $container) {
    $url = HTTP_SERVER . $_SERVER['REQUEST_URI'];

    return new App\Engine\Router\Router(
        $url,
        \App\Engine\Router\RouteCollection::getInstance(),
        $container->get('userService'),
        $container->get('session')
    );
};
$container['session'] = function() {
    return new App\Engine\Session\Session();
};
$container['cookie'] = function() {
    return new App\Engine\Cookie();
};
$container['translation'] = function(\Psr\Container\ContainerInterface $container) {
    return new App\Engine\Translation($container->get('cookie'));
};
$container['view'] = function(\Psr\Container\ContainerInterface $container) {
    return new App\Engine\View(
        $container->get('translation'),
        $container->get('session'),
        $container->get('cookie'),
        $container->get('router'),
        $container->get('settings')->all(),
        $container->get('routeService')
    );
};
$container['cache'] = function() {
    return new \Symfony\Component\Cache\Simple\FilesystemCache();
};
/**
 * Users
 */
$container['userSessionService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\User\UserSessionService($container->get('session'));
};
$container['userCommandRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\User\UserCommandRepository($container->get('db'));
};
$container['userQueryRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\User\UserQueryRepository($container->get('db'));
};
$container['userService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\User\UserService(
        $container->get('userSessionService'),
        $container->get('userCommandRepository'),
        $container->get('userQueryRepository')
    );
};
/**
 * Category
 */
$container['categoryCommandRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Category\CategoryCommandRepository($container->get('db'));
};
$container['categoryQueryRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Category\CategoryQueryRepository($container->get('db'));
};
$container['categoryService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Category\CategoryService(
        $container->get('categoryCommandRepository'),
        $container->get('categoryQueryRepository')
    );
};
/**
 * Places
 */
$container['placeCommandRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Place\PlaceCommandRepository($container->get('db'));
};
$container['placeQueryRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Place\PlaceQueryRepository($container->get('db'));
};
$container['placeService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Place\PlaceService(
        $container->get('placeCommandRepository'),
        $container->get('placeQueryRepository')
    );
};
/**
 * Product
 */
$container['productCommandRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Product\ProductCommandRepository($container->get('db'));
};
$container['productQueryRepository'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Product\ProductQueryRepository($container->get('db'));
};
$container['productService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Product\ProductService(
        $container->get('productCommandRepository'),
        $container->get('productQueryRepository')
    );
};
/**
 * Route
 */
$container['routeService'] = function(\Psr\Container\ContainerInterface $container) {
    return new \App\Src\Route\RouteService($container->get('session'));
};
