<?php
use App\Engine\Router\RouteCollection;
use App\Engine\Router\Route;

$collection = RouteCollection::getInstance();
$collection->add('homePage', new Route(
    HTTP_SERVER. '/',
    [
        'class'     => '\Frontend\HomePage',
        'method'    => 'index',
    ]
));
$collection->add('adminHomePage', new Route(
    HTTP_SERVER. '/admin/',
    [
        'class'     => '\Admin\Index',
        'method'    => 'index',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));
$collection->add('404', new Route(
    HTTP_SERVER . '/404',
    [
        'class'  => '\Error',
        'method' => 'error404',
    ]
));
$collection->add('login', new Route(
    HTTP_SERVER . '/login',
    [
        'class'  => '\Login',
        'method' => 'loginAction',
    ]
));
$collection->add('logout', new Route(
    HTTP_SERVER . '/logout',
    [
        'class'  => '\Login',
        'method' => 'logOutAction',
    ]
));
$collection->add('changeLang', new Route(
    HTTP_SERVER . '/language/<lang>',
    [
        'class'  => '\Language',
        'method' => 'change',
    ],
    [
        'lang' => '(\w{2,2})$',
    ]
));
$collection->add('changeTheme', new Route(
    HTTP_SERVER . '/theme',
    [
        'class'  => '\Template',
        'method' => 'switchTheme',
    ]
));
$collection->add('categoryIndex', new Route(
    HTTP_SERVER . '/admin/category',
    [
        'class'     => '\Admin\Category',
        'method'    => 'viewCategories',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));
$collection->add('categoryAddNew', new Route(
    HTTP_SERVER . '/admin/category/add',
    [
        'class'     => '\Admin\Category',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));
$collection->add('categoryDetails', new Route(
    HTTP_SERVER . '/admin/category/<id>',
    [
        'class'     => '\Admin\Category',
        'method'    => 'showDetails',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'id' => '([1-9][0-9]*)',
    ]
));
$collection->add('placeAddNew', new Route(
    HTTP_SERVER . '/admin/place/add',
    [
        'class'     => '\Admin\Place',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));
$collection->add('placeAddNewForCategory', new Route(
    HTTP_SERVER . '/admin/category/<categoryId>/place/add',
    [
        'class'     => '\Admin\Place',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'categoryId' => '([1-9][0-9]*)',
    ]
));
$collection->add('placeDelete', new Route(
    HTTP_SERVER . '/admin/place/delete/<id>',
    [
        'class'     => '\Admin\Place',
        'method'    => 'deletePlace',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'id' => '([1-9][0-9]*)',
    ]
));
$collection->add('placeDetails', new Route(
    HTTP_SERVER . '/admin/place/<id>',
    [
        'class'     => '\Admin\Place',
        'method'    => 'details',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'id' => '([1-9][0-9]*)',
    ]
));
$collection->add('addProductForPlace', new Route(
    HTTP_SERVER . '/admin/place/<placeId>/product/add',
    [
        'class'     => '\Admin\Product',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ],
    [
        'placeId' => '([1-9][0-9]*)',
    ]
));
$collection->add('addProduct', new Route(
    HTTP_SERVER . '/admin/product/add',
    [
        'class'     => '\Admin\Product',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::LOGGED,
    ]
));

$collection->add('getProducts', new Route(
    HTTP_SERVER . '/admin/place/<placeId>/products',
    [
        'class'     => '\Admin\Product',
        'method'    => 'getProducts',
    ],
    [
        'placeId' => '([1-9][0-9]*)',
    ],
    [
        'page' => '1',
    ]
));
$collection->add('getProductsPage', new Route(
    HTTP_SERVER . '/admin/place/<placeId>/products/<page>',
    [
        'class'     => '\Admin\Product',
        'method'    => 'getProducts',
    ],
    [
        'placeId' => '([1-9][0-9]*)',
        'page' => '([1-9][0-9]*)',
    ]
));
$collection->add('adminUsersList', new Route(
    HTTP_SERVER . '/admin/users',
    [
        'class'     => '\Admin\User',
        'method'    => 'viewUsers',
        'authLevel' => \App\Src\User\UserConst::ADMIN,
    ]
));
$collection->add('adminUsersAdd', new Route(
    HTTP_SERVER . '/admin/user/add',
    [
        'class'     => '\Admin\User',
        'method'    => 'add',
        'authLevel' => \App\Src\User\UserConst::ADMIN,
    ]
));

$collection->add('getAllPlacesAjax', new Route(
    HTTP_SERVER . '/ajax/places',
    [
        'class'     => '\Frontend\Place',
        'method'    => 'getAllAjax',
    ]
));

$collection->add('getPlacesByCategoryAjax', new Route(
    HTTP_SERVER . '/ajax/places/category/<categoryId>',
    [
        'class'     => '\Frontend\Place',
        'method'    => 'getByCategoryAjax',
    ],[
        'categoryId' => '([1-9][0-9]*)',
    ]
));
$collection->add('categoryById', new Route(
    HTTP_SERVER . '/category/<categoryId>',
    [
        'class'     => '\Frontend\Category',
        'method'    => 'getById',
    ],[
        'categoryId' => '([1-9][0-9]*)',
    ]
));
$collection->add('placeById', new Route(
    HTTP_SERVER . '/place/<placeId>',
    [
        'class'     => '\Frontend\Place',
        'method'    => 'getById',
    ],[
        'placeId' => '([1-9][0-9]*)',
    ]
));
$collection->add('bestPrice', new Route(
    HTTP_SERVER . '/product/best',
    [
        'class'     => '\Frontend\Product',
        'method'    => 'bestPrice',
    ]
));
$collection->add('bestPriceAjax', new Route(
    HTTP_SERVER . '/ajax/product/best',
    [
        'class'     => '\Frontend\Product',
        'method'    => 'bestPriceAjax',
    ]
));
$collection->add('setYourLocalization', new Route(
    HTTP_SERVER . '/route/localization/you',
    [
        'class'     => '\Frontend\Route',
        'method'    => 'setYourLocalization',
    ]
));
$collection->add('addPlaceToRoute', new Route(
    HTTP_SERVER . '/route/localization/add/<placeId>',
    [
        'class'     => '\Frontend\Route',
        'method'    => 'addToRoute',
    ],[
        'placeId' => '([1-9][0-9]*)',
    ]
));
$collection->add('ajaxRoute', new Route(
    HTTP_SERVER . '/ajax/route',
    [
        'class'     => '\Frontend\Route',
        'method'    => 'ajaxGetRoute',
    ]
));
$collection->add('removeRoute', new Route(
    HTTP_SERVER . '/route/remove',
    [
        'class'     => '\Frontend\Route',
        'method'    => 'removeYourLocalization',
    ]
));
$collection->add('viewRoute', new Route(
    HTTP_SERVER . '/route',
    [
        'class'     => '\Frontend\Route',
        'method'    => 'viewRoute',
    ]
));