<?php
declare(strict_types=1);
namespace App\Src\User;

/**
 * Class UserConst
 *
 * @package App\Src\User
 */
class UserConst
{
    /**
     * @const int
     */
    CONST ANONYMOUS = 0;

    /**
     * @const int
     */
    CONST LOGGED = 10;

    /**
     * @const int
     */
    CONST ADMIN = 50;

    /**
     * @const int
     */
    CONST SUPER_ADMIN = 100;

    /**
     * @const string
     */
    CONST ANONYMOUS_NAME = 'Anonymous';

    /**
     * @const int
     */
    CONST LOGGED_NAME = 'User';

    /**
     * @const int
     */
    CONST ADMIN_NAME = 'Admin';

    /**
     * @const int
     */
    CONST SUPER_ADMIN_NAME = 'Super admin';

    /**
     * @const array
     */
    const USER_NAMES = [
        self::ANONYMOUS   => self::ANONYMOUS_NAME,
        self::LOGGED      => self::LOGGED_NAME,
        self::ADMIN       => self::ADMIN_NAME,
        self::SUPER_ADMIN => self::SUPER_ADMIN_NAME,
    ];

    /**
     * @const array
     */
    const USER_VALUES = [
        self::ANONYMOUS_NAME   => self::ANONYMOUS,
        self::LOGGED_NAME      => self::LOGGED,
        self::ADMIN_NAME       => self::ADMIN,
        self::SUPER_ADMIN_NAME => self::SUPER_ADMIN,
    ];
}
