<?php
declare(strict_types=1);
namespace App\Src\User;

use Exception;

/**
 * Class UserCommandRepository
 *
 * @package App\Domains\User
 */
class UserCommandRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * UserCommandRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param UserCreateCommand $userCreateCommand
     *
     * @return int
     * @throws Exception
     */
    public function create(UserCreateCommand $userCreateCommand) : int
    {
        $statement = $this->db->prepare('INSERT INTO users (
            email,
            password,
            level,
            created_by
        ) VALUES (
            :emial,
            :password,
            :level,
            :createdBy
        )');
        $result = $statement->execute([
            'emial'     => $userCreateCommand->getEmail(),
            'password'  => $userCreateCommand->getPassword(),
            'level'     => $userCreateCommand->getLevel(),
            'createdBy' => $userCreateCommand->getCreatedBy(),
        ]);
        if (!$result) {
            throw new Exception('Cant create new user');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param int $userId
     *
     * @return int
     * @throws \Exception
     */
    public function updateLastLogin(int $userId) : int
    {
        $statement = $this->db->prepare('UPDATE users SET last_login = NOW() WHERE id = :id');
        $result = $statement->execute([
            'id' => $userId,
        ]);
        if (!$result) {
            throw new Exception('Cant update user');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return int
     * @throws \Exception
     */
    public function updateUserPassword(string $email, string $password) : int
    {
        $statement = $this->db->prepare('UPDATE users SET password = :password WHERE email = :email');
        $result = $statement->execute([
            'password' => $password,
            'email'    => $email,
        ]);
        if (!$result) {
            throw new Exception('Cant update user');
        }

        return (int) $this->db->lastInsertId();
    }
}
