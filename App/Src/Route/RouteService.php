<?php
declare(strict_types=1);
namespace App\Src\Route;

use App\Engine\Session\Session;
use App\Src\Localization\LocalizationModel;
use App\Src\Localization\LocalizationModelCollection;

/**
 * Class RouteService
 *
 * @package App\Src\Route
 */
class RouteService
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @const string
     */
    protected const ROUTE_LOCALIZATION_SESSION_KEY = 'route';

    /**
     * RouteService constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param LocalizationModelCollection $localizationModelCollection
     *
     * @return RouteService
     */
    public function setRoute(LocalizationModelCollection $localizationModelCollection) : self
    {
        $this->session->set(self::ROUTE_LOCALIZATION_SESSION_KEY, serialize($localizationModelCollection));

        return $this;
    }

    /**
     * @return LocalizationModelCollection
     */
    public function getRoute() : LocalizationModelCollection
    {
        if (!$this->session->has(self::ROUTE_LOCALIZATION_SESSION_KEY)) {

            return new LocalizationModelCollection();
        }

        return unserialize($this->session->get(self::ROUTE_LOCALIZATION_SESSION_KEY));
    }

    /**
     * @param LocalizationModel $localizationModel
     *
     * @return RouteService
     * @throws \ReflectionException
     */
    public function addToRoute(LocalizationModel $localizationModel) : self
    {
        return $this->setRoute($this->getRoute()->addToCollection($localizationModel));
    }

    /**
     * @return RouteService
     */
    public function reset() : self
    {
        return $this->setRoute(new LocalizationModelCollection());
    }
}
