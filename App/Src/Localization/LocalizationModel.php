<?php
declare(strict_types=1);
namespace App\Src\Localization;

use App\Engine\Model;

/**
 * Class LocalizationModel
 *
 * @package App\Src\Localization
 */
class LocalizationModel extends Model
{
    /**
     * @var string
     */
    protected $latitude;

    /**
     * @var string
     */
    protected $longitude;

    /**
     * @var string
     */
    protected $address;

    /**
     * @return string
     */
    public function getLatitude() : string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     *
     * @return LocalizationModel
     */
    public function setLatitude(string $latitude) : self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getLongitude() : string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     *
     * @return LocalizationModel
     */
    public function setLongitude(string $longitude) : self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress() : string
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return LocalizationModel
     */
    public function setAddress(string $address) : self
    {
        $this->address = $address;

        return $this;
    }
}
