<?php
declare(strict_types=1);
namespace App\Src\Localization;

use App\Engine\Common\Collection;

/**
 * Class LocalizationModelCollection
 *
 * @package App\Src\Localization
 *
 * @method LocalizationModel current()
 * @method LocalizationModelCollection addToCollection(LocalizationModel $localizationModel)
 * @method LocalizationModel getFirst()
 */
class LocalizationModelCollection extends Collection
{
    /**
     * LocalizationModelCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(LocalizationModel::class);
    }
}
