<?php
declare(strict_types=1);
namespace App\Src\Category;

/**
 * Class CategoryConst
 *
 * @package App\Src\Category
 */
class CategoryConst
{
    /**
     * @const array
     */
    public const MARKER_COLORS = [
        'blue'   => 'blue',
        'red'    => 'red',
        'purple' => 'purple',
        'green'  => 'green',
        'yellow' => 'yellow',
    ];
}
