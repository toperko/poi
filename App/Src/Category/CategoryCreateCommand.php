<?php
declare(strict_types=1);
namespace App\Src\Category;

use App\Engine\Command;

/**
 * Class CategoryCreateCommand
 *
 * @package App\Src\Category
 */
class CategoryCreateCommand extends Command
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $markerColor;

    /**
     * @var int
     */
    private $createdBy;

    /**
     * @return bool
     */
    public function valid() : bool
    {
        if (empty($this->name)) {
            $this->errors['name'] = 'Name cant be empty';
        }
        if (!empty($this->name) && strlen($this->name) < 3 || strlen($this->name) > 200) {
            $this->errors['nameLength'] = 'Name must contain from 3 to 200 characters';
        }

        return count($this->errors) === 0;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CategoryCreateCommand
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return CategoryCreateCommand
     */
    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getMarkerColor() : string
    {
        return $this->markerColor;
    }

    /**
     * @param string $markerColor
     *
     * @return CategoryCreateCommand
     */
    public function setMarkerColor(string $markerColor) : self
    {
        $this->markerColor = $markerColor;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy() : int
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     *
     * @return CategoryCreateCommand
     */
    public function setCreatedBy(int $createdBy) : self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
