<?php
declare(strict_types=1);
namespace App\Src\Category;

/**
 * Class PageCommandRepository
 *
 * @package App\Src\Page
 */
class CategoryCommandRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * PageCommandRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param CategoryCreateCommand $pageCreateCommand
     *
     * @return int
     * @throws \Exception
     */
    public function create(CategoryCreateCommand $pageCreateCommand) : int
    {
        $statement = $this->db->prepare('INSERT INTO categories (
            name,
            description,
            marker_color,
            created_by
        ) VALUES (
            :name,
            :description,
            :markerColor,
            :createdBy
        )');
        $result = $statement->execute([
            'name'        => $pageCreateCommand->getName(),
            'description' => $pageCreateCommand->getDescription(),
            'markerColor' => $pageCreateCommand->getMarkerColor(),
            'createdBy'   => $pageCreateCommand->getCreatedBy(),
        ]);
        if (!$result) {
            throw new \Exception('Cant create new category');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param int $id
     *
     * @return int
     * @throws \Exception
     */
    public function delete(int $id) : int
    {
        $statement = $this->db->prepare('UPDATE categories SET deleted_at = NOW() WHERE id = :id');
        $result = $statement->execute([
            'id' => $id,
        ]);
        if (!$result) {
            throw new \Exception('Cant delete category');
        }

        return (int) $this->db->lastInsertId();
    }
}
