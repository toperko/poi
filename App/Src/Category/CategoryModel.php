<?php
declare(strict_types=1);
namespace App\Src\Category;

use App\Engine\Model;

/**
 * Class CategoryModel
 *
 * @package App\Src\Category
 */
class CategoryModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $markerColor;

    /**
     * @var int
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * CategoryModel constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setId($data['id']);
        $this->setName($data['name']);
        $this->setDescription($data['description']);
        $this->setMarkerColor($data['marker_color']);
        $this->setCreatedBy($data['created_by']);
        $this->setCreatedAt(new \DateTime($data['created_at']));
        $this->setDeletedAt(isset($data['deleted_at']) ? new \DateTime($data['deleted_at']) : null);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return CategoryModel
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CategoryModel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return CategoryModel
     */
    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getMarkerColor() : string
    {
        return $this->markerColor;
    }

    /**
     * @param string $markerColor
     *
     * @return CategoryModel
     */
    public function setMarkerColor(string $markerColor) : self
    {
        $this->markerColor = $markerColor;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy() : int
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     *
     * @return CategoryModel
     */
    public function setCreatedBy(int $createdBy) : self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt() : \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return CategoryModel
     */
    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt() : ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     *
     * @return CategoryModel
     */
    public function setDeletedAt(?\DateTime $deletedAt) : self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
