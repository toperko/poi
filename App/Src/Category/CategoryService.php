<?php
declare(strict_types=1);
namespace App\Src\Category;

use App\Engine\Exception\ValidationException;

/**
 * Class PageService
 *
 * @package App\Src\Page
 */
class CategoryService
{
    /**
     * @var CategoryCommandRepository
     */
    private $categoryCommandRepository;

    /**
     * @var CategoryQueryRepository
     */
    private $categoryQueryRepository;

    /**
     * CategoryService constructor.
     *
     * @param CategoryCommandRepository $categoryCommandRepository
     * @param CategoryQueryRepository   $categoryQueryRepository
     */
    public function __construct(CategoryCommandRepository $categoryCommandRepository, CategoryQueryRepository $categoryQueryRepository)
    {
        $this->categoryCommandRepository = $categoryCommandRepository;
        $this->categoryQueryRepository = $categoryQueryRepository;
    }

    /**
     * @return CategoryModelCollection
     * @throws \ReflectionException
     */
    public function getAll() : CategoryModelCollection
    {
        return $this->categoryQueryRepository->getAll();
    }

    /**
     * @param int $userId
     *
     * @return CategoryModelCollection
     * @throws \Exception
     */
    public function getCategoriesCreatedBy(int $userId) : CategoryModelCollection
    {
        return $this->categoryQueryRepository->getCategoriesCreatedBy($userId);
    }

    /**
     * @param int $id
     *
     * @return CategoryModel|null
     * @throws \Exception
     */
    public function getById(int $id) : ?CategoryModel
    {
        return $this->categoryQueryRepository->getById($id);
    }

    /**
     * @param CategoryCreateCommand $categoryCreateCommand
     *
     * @return int
     * @throws ValidationException
     * @throws \Exception
     */
    public function create(CategoryCreateCommand $categoryCreateCommand) : int
    {
        if(!$categoryCreateCommand->valid()){
            throw new ValidationException('Cant create category ', implode(', ', $categoryCreateCommand->getErrors()));
        }
        return $this->categoryCommandRepository->create($categoryCreateCommand);
    }

    /**
     * @param int $id
     *
     * @return int
     * @throws \Exception
     */
    public function delete(int $id) : int
    {
        $page = $this->getById($id);
        if(!$page){
            throw new \Exception('Cant find this category');
        }
        if($page->getDeletedAt()){
            throw new \Exception('Category is already deleted');
        }

        return $this->categoryCommandRepository->delete($id);
    }
}
