<?php
declare(strict_types=1);
namespace App\Src\Category;

/**
 * Class CategoryQueryRepository
 *
 * @package App\Src\Category
 */
class CategoryQueryRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * CategoryQueryRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $categoryId
     *
     * @return CategoryModel|null
     * @throws \Exception
     */
    public function getById(int $categoryId) : ?CategoryModel
    {
        $statement = $this->db->prepare('SELECT
                id,
                name,
                description,
                marker_color,
                created_by,
                created_at,
                deleted_at
            FROM categories WHERE id = :categoryId;
        ');
        $result = $statement->execute([
            'categoryId' => $categoryId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from categories');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new CategoryModel($result);
    }

    /**
     * @return CategoryModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getAll() : CategoryModelCollection
    {
        $categoryModelCollection = new CategoryModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                name,
                description,
                marker_color,
                created_by,
                created_at,
                deleted_at
            FROM categories;
        ');
        $result = $statement->execute([]);
        if (!$result) {
            throw new \Exception('Cant get data from categories');
        }
        $result = $statement->fetchAll();

        return $categoryModelCollection->buildFromArray($result);
    }

    /**
     * @param int $userId
     *
     * @return CategoryModelCollection
     * @throws \Exception
     */
    public function getCategoriesCreatedBy(int $userId) : CategoryModelCollection
    {
        $categoryModelCollection = new CategoryModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                name,
                description,
                marker_color,
                created_by,
                created_at,
                deleted_at
            FROM categories 
            WHERE created_by = :userId
            AND deleted_at IS NULL;
        ');
        $result = $statement->execute([
            'userId' => $userId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from pages');
        }
        $result = $statement->fetchAll();

        return $categoryModelCollection->buildFromArray($result);
    }
}
