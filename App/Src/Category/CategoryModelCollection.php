<?php
declare(strict_types=1);
namespace App\Src\Category;

use App\Engine\Common\Collection;

/**
 * Class CategoryModelCollection
 *
 * @package App\Src\Category
 * @method CategoryModel current()
 * @method CategoryModel getByKey($key)
 */
class CategoryModelCollection extends Collection
{
    /**
     * CategoryModelCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(CategoryModel::class, null, 'id');
    }
}
