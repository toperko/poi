<?php
declare(strict_types=1);
namespace App\Src\Place;

use App\Engine\Command;

/**
 * Class PlaceCreateCommand
 *
 * @package App\Src\Place
 */
class PlaceCreateCommand extends Command
{
    /**
     * @var int
     */
    private $categoryId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $createdBy;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string|null
     */
    private $img;

    /**
     * @var string
     */
    private $latitude;

    /**
     * @var string
     */
    private $longitude;

    /**
     * @var string
     */
    private $address;

    /**
     * @return bool
     */
    public function valid() : bool
    {
        if (empty($this->categoryId) || $this->pageId === 0) {
            $this->errors['category'] = 'Category cant be empty';
        }
        if (empty($this->name)) {
            $this->errors['name'] = 'Name cant be empty';
        }
        if (!empty($this->name) && strlen($this->name) < 3 || strlen($this->name) > 200) {
            $this->errors['nameLength'] = 'Name must contain from 3 to 200 characters';
        }
        if (empty($this->description)) {
            $this->errors['description'] = 'Description cant be empty';
        }
        if (!empty($this->description) && strlen($this->description) < 3 || strlen($this->description) > 200) {
            $this->errors['description'] = 'Description must contain from 3 to 200 characters';
        }
        if (empty($this->latitude) || empty($this->latitude)) {
            $this->errors['position'] = 'Position cant be empty';
        }

        return count($this->errors) === 0;
    }

    /**
     * @return int
     */
    public function getCategoryId() : int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     *
     * @return PlaceCreateCommand
     */
    public function setCategoryId(int $categoryId) : self
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PlaceCreateCommand
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy() : int
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     *
     * @return PlaceCreateCommand
     */
    public function setCreatedBy(int $createdBy) : self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return PlaceCreateCommand
     */
    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getImg() : ?string
    {
        return $this->img;
    }

    /**
     * @param null|string $img
     *
     * @return PlaceCreateCommand
     */
    public function setImg(?string $img) : self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return string
     */
    public function getLatitude() : string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     *
     * @return PlaceCreateCommand
     */
    public function setLatitude(string $latitude) : self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getLongitude() : string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     *
     * @return PlaceCreateCommand
     */
    public function setLongitude(string $longitude) : self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress() : string
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return PlaceCreateCommand
     */
    public function setAddress(string $address) : self
    {
        $this->address = $address;

        return $this;
    }
}
