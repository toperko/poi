<?php
declare(strict_types=1);
namespace App\Src\Place;

use App\Src\Localization\LocalizationModel;

/**
 * Class PlaceModel
 *
 * @package App\Src\Place
 */
class PlaceModel extends LocalizationModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $categoryId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var int
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string|null
     */
    private $img;

    /**
     * PlaceModel constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setId($data['id']);
        $this->setCategoryId($data['category_id']);
        $this->setName($data['name']);
        $this->setCreatedAt(new \DateTime($data['created_at']));
        $this->setCreatedBy($data['created_by']);
        $this->setDeletedAt(isset($data['deleted_at']) ? new \DateTime($data['deleted_at']) : null);
        $this->setDescription($data['description']);
        $this->setImg($data['img']);
        $this->setLatitude($data['latitude']);
        $this->setLongitude($data['longitude']);
        $this->setAddress($data['address']);
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id'          => $this->getId(),
            'categoryId'  => $this->getCategoryId(),
            'name'        => $this->getName(),
            'cratedAt'    => $this->getCreatedAt()->format('Y-m-d H:i:s'),
            'createdBy'   => $this->getCreatedBy(),
            'deletedAt'   => $this->getDeletedAt() ? $this->getDeletedAt()->format('Y-m-d H:i:s') : null,
            'description' => $this->getDescription(),
            'img'         => $this->getImg(),
            'latitude'    => $this->getLatitude(),
            'longitude'   => $this->getLongitude(),
            'address'     => $this->getAddress(),
        ];
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return PlaceModel
     */
    public function setId(int $id) : self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getCategoryId() : int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     *
     * @return PlaceModel
     */
    public function setCategoryId(int $categoryId) : self
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PlaceModel
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt() : \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return PlaceModel
     */
    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy() : int
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     *
     * @return PlaceModel
     */
    public function setCreatedBy(int $createdBy) : self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt() : ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     *
     * @return PlaceModel
     */
    public function setDeletedAt(?\DateTime $deletedAt) : self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return PlaceModel
     */
    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getImg() : ?string
    {
        return $this->img;
    }

    /**
     * @param null|string $img
     *
     * @return PlaceModel
     */
    public function setImg(?string $img) : self
    {
        $this->img = $img;

        return $this;
    }
}
