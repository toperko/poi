<?php
declare(strict_types=1);
namespace App\Src\Place;

use App\Engine\Common\Collection;

/**
 * Class PlaceModelCollection
 *
 * @package App\Src\Event
 *
 * @method PlaceModel current()
 */
class PlaceModelCollection extends Collection
{
    /**
     * PlaceModelCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(PlaceModel::class, null, 'id');
    }
}
