<?php
declare(strict_types=1);
namespace App\Src\Place;

/**
 * Class PlaceQueryRepository
 *
 * @package App\Src\Place
 */
class PlaceQueryRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * PlaceQueryRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $placeId
     *
     * @return PlaceModel|null
     * @throws \Exception
     */
    public function getById(int $placeId) : ?PlaceModel
    {
        $statement = $this->db->prepare('SELECT
                id,
                category_id,
                name,
                created_at,
                created_by,
                deleted_at,
                description,
                img,
                latitude,
                longitude,
                address
            FROM places WHERE id = :placeId;
        ');
        $result = $statement->execute([
            'placeId' => $placeId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from places');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new PlaceModel($result);
    }

    /**
     * @param int $categoryId
     *
     * @return PlaceModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getByCategoryId(int $categoryId) : PlaceModelCollection
    {
        $placeModelCollection = new PlaceModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                category_id,
                name,
                created_at,
                created_by,
                deleted_at,
                description,
                img,
                latitude,
                longitude,
                address
            FROM places 
            WHERE category_id = :categoryId
            AND deleted_at IS NULL;
        ');
        $result = $statement->execute([
            'categoryId' => $categoryId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from places');
        }
        $result = $statement->fetchAll();

        return $placeModelCollection->buildFromArray($result);
    }

    /**
     * @return PlaceModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getAll() : PlaceModelCollection
    {
        $placeModelCollection = new PlaceModelCollection();
        $statement = $this->db->prepare('SELECT
                id,
                category_id,
                name,
                created_at,
                created_by,
                deleted_at,
                description,
                img,
                latitude,
                longitude,
                address
            FROM places 
            WHERE deleted_at IS NULL;
        ');
        $result = $statement->execute([]);
        if (!$result) {
            throw new \Exception('Cant get data from places');
        }
        $result = $statement->fetchAll();

        return $placeModelCollection->buildFromArray($result);
    }
}
