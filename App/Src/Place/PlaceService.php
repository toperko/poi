<?php
declare(strict_types=1);
namespace App\Src\Place;

use App\Engine\Exception\ValidationException;

/**
 * Class PlaceService
 *
 * @package App\Src\Place
 */
class PlaceService
{
    /**
     * @var PlaceCommandRepository
     */
    private $placeCommandRepository;

    /**
     * @var PlaceQueryRepository
     */
    private $placeQueryRepository;

    /**
     * PlaceService constructor.
     *
     * @param PlaceCommandRepository $placeCommandRepository
     * @param PlaceQueryRepository   $placeQueryRepository
     */
    public function __construct(PlaceCommandRepository $placeCommandRepository, PlaceQueryRepository $placeQueryRepository)
    {
        $this->placeCommandRepository = $placeCommandRepository;
        $this->placeQueryRepository = $placeQueryRepository;
    }

    /**
     * @param PlaceCreateCommand $placeCreateCommand
     *
     * @return int
     * @throws ValidationException
     * @throws \Exception
     */
    public function create(PlaceCreateCommand $placeCreateCommand) : int
    {
        if (!$placeCreateCommand->valid()) {
            throw new ValidationException('Cant create place ', implode(', ', $placeCreateCommand->getErrors()));
        }

        return $this->placeCommandRepository->create($placeCreateCommand);
    }

    /**
     * @param int $placeId
     *
     * @return PlaceModel|null
     * @throws \Exception
     */
    public function getById(int $placeId) : ?PlaceModel
    {
        return $this->placeQueryRepository->getById($placeId);
    }

    /**
     * @return PlaceModelCollection
     * @throws \ReflectionException
     */
    public function getAll() : PlaceModelCollection
    {
        return $this->placeQueryRepository->getAll();
    }

    /**
     * @param int $id
     *
     * @return PlaceModelCollection
     * @throws \ReflectionException
     */
    public function getByCategoryId(int $id) : PlaceModelCollection
    {
        return $this->placeQueryRepository->getByCategoryId($id);
    }

    /**
     * @param int $id
     *
     * @return int
     * @throws \Exception
     */
    public function delete(int $id) : int
    {
        $event = $this->getById($id);
        if (!$event) {
            throw new \Exception('Cant find this place');
        }
        if ($event->getDeletedAt()) {
            throw new \Exception('Place is already deleted');
        }

        return $this->placeCommandRepository->delete($id);
    }

    /**
     * @param int    $id
     * @param string $img
     *
     * @return int
     * @throws \Exception
     */
    public function updateImg(int $id, string $img) : int
    {
        return $this->placeCommandRepository->updateImg($id, $img);
    }
}
