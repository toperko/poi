<?php
declare(strict_types=1);
namespace App\Src\Place;

/**
 * Class PlaceCommandRepository
 *
 * @package App\Src\Place
 */
class PlaceCommandRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * PlaceCommandRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param PlaceCreateCommand $placeCreateCommand
     *
     * @return int
     * @throws \Exception
     */
    public function create(PlaceCreateCommand $placeCreateCommand) : int
    {
        $statement = $this->db->prepare('INSERT INTO places (
            category_id,
            name,
            created_by,
            description,
            img,
            latitude,
            longitude,
            address
        ) VALUES (
            :categoryId,
            :name,
            :createdBy,
            :description,
            :img,
            :latitude,
            :longitude,
            :address
        )');
        $result = $statement->execute([
            'categoryId'  => $placeCreateCommand->getCategoryId(),
            'name'        => $placeCreateCommand->getName(),
            'createdBy'   => $placeCreateCommand->getCreatedBy(),
            'description' => $placeCreateCommand->getDescription(),
            'img'         => $placeCreateCommand->getImg(),
            'latitude'    => $placeCreateCommand->getLatitude(),
            'longitude'   => $placeCreateCommand->getLongitude(),
            'address'     => $placeCreateCommand->getAddress(),
        ]);
        if (!$result) {
            throw new \Exception('Cant create new place');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param int    $id
     * @param string $img
     *
     * @return int
     * @throws \Exception
     */
    public function updateImg(int $id, string $img) : int
    {
        $statement = $this->db->prepare('UPDATE places SET img = :img WHERE id = :id');
        $result = $statement->execute([
            'img' => $img,
            'id'  => $id,
        ]);
        if (!$result) {
            throw new \Exception('Cant update palce');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param int $id
     *
     * @return int
     * @throws \Exception
     */
    public function delete(int $id) : int
    {
        $statement = $this->db->prepare('UPDATE places SET deleted_at = NOW() WHERE id = :id');
        $result = $statement->execute([
            'id' => $id,
        ]);
        if (!$result) {
            throw new \Exception('Cant delete palce');
        }

        return (int) $this->db->lastInsertId();
    }
}
