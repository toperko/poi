<?php
declare(strict_types=1);
namespace App\Src\Product;

use App\Engine\Common\Collection;

/**
 * Class ProductModelCollection
 *
 * @package App\Src\Product
 * @method ProductModel current()
 */
class ProductModelCollection extends Collection
{
    /**
     * ProductModelCollection constructor.
     */
    public function __construct()
    {
        parent::__construct(ProductModel::class);
    }
}
