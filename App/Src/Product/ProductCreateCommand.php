<?php
declare(strict_types=1);
namespace App\Src\Product;

use App\Engine\Command;

/**
 * Class ProductCreateCommand
 *
 * @package App\Src\Product
 */
class ProductCreateCommand extends Command
{
    /**
     * @var int
     */
    private $placeId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $price;

    /**
     * @var int
     */
    private $createdBy;

    /**
     * @return bool
     */
    public function valid() : bool
    {
        return count($this->errors) === 0;
    }

    /**
     * @return int
     */
    public function getPlaceId() : int
    {
        return $this->placeId;
    }

    /**
     * @param int $placeId
     *
     * @return ProductCreateCommand
     */
    public function setPlaceId(int $placeId) : self
    {
        $this->placeId = $placeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ProductCreateCommand
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice() : int
    {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return ProductCreateCommand
     */
    public function setPrice(int $price) : self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy() : int
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     *
     * @return ProductCreateCommand
     */
    public function setCreatedBy(int $createdBy) : self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
