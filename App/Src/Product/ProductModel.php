<?php
declare(strict_types=1);
namespace App\Src\Product;

use App\Engine\Model;

/**
 * Class ProductModel
 *
 * @package App\Src\Product
 */
class ProductModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $placeId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $price;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var int
     */
    private $createdBy;

    /**
     * ProductModel constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setId($data['id']);
        $this->setPlaceId($data['place_id']);
        $this->setName($data['name']);
        $this->setPrice($data['price']);
        $this->setCreatedAt(new \DateTime($data['created_at']));
        $this->setCreatedBy($data['created_by']);
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id'          => $this->getId(),
            'placeId'     => $this->getPlaceId(),
            'name'        => $this->getName(),
            'price'       => $this->getPrice(),
            'priceString' => $this->getPriceAsString(),
            'createdAt'   => $this->getCreatedAt()->format('Y-m-d H:i:s'),
            'createdBy'   => $this->getCreatedBy(),
        ];
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ProductModel
     */
    public function setId(int $id) : self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getPlaceId() : int
    {
        return $this->placeId;
    }

    /**
     * @param int $placeId
     *
     * @return ProductModel
     */
    public function setPlaceId(int $placeId) : self
    {
        $this->placeId = $placeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ProductModel
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice() : int
    {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return ProductModel
     */
    public function setPrice(int $price) : self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt() : \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return ProductModel
     */
    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy() : int
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     *
     * @return ProductModel
     */
    public function setCreatedBy(int $createdBy) : self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getPriceAsString() : string
    {
        return number_format(floatval(strval($this->getPrice() / 100)), 2);
    }
}
