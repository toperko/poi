<?php
declare(strict_types=1);
namespace App\Src\Product;

/**
 * Class ProductCommandRepository
 *
 * @package App\Src\Product
 */
class ProductCommandRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * ProductCommandRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param ProductCreateCommand $productCreateCommand
     *
     * @return int
     * @throws \Exception
     */
    public function create(ProductCreateCommand $productCreateCommand) : int
    {
        $statement = $this->db->prepare('INSERT INTO products (
            place_id,
            name,
            price,
            created_by
        ) VALUES (
            :placeId,
            :name,
            :price,
            :createdBy
        )');
        $result = $statement->execute([
            'placeId'   => $productCreateCommand->getPlaceId(),
            'name'      => $productCreateCommand->getName(),
            'price'     => $productCreateCommand->getPrice(),
            'createdBy' => $productCreateCommand->getCreatedBy(),
        ]);
        if (!$result) {
            throw new \Exception('Cant create new event call');
        }

        return (int) $this->db->lastInsertId();
    }

    /**
     * @param int $placeId
     *
     * @return int
     */
    public function deleteByPlaceId(int $placeId) : int
    {
        $statement = $this->db->prepare('DELETE FROM products WHERE place_id = :placeId');
        $statement->execute([
            'placeId' => $placeId,
        ]);

        return $statement->rowCount();
    }
}
