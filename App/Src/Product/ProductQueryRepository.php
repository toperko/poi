<?php
declare(strict_types=1);
namespace App\Src\Product;

/**
 * Class ProductQueryRepository
 *
 * @package App\Src\Product
 */
class ProductQueryRepository
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * ProductQueryRepository constructor.
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @return ProductModel|null
     * @throws \Exception
     */
    public function getLast() : ?ProductModel
    {
        $statement = $this->db->prepare('SELECT
                id,
                place_id,
                name,
                price,
                created_at,
                created_by
            FROM products
            ORDER BY id DESC LIMIT 1;
        ');
        $result = $statement->execute([]);
        if (!$result) {
            throw new \Exception('Cant get data from products');
        }
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new ProductModel($result);
    }

    /**
     * @param int      $placeId
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return ProductModelCollection
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getByPlaceId(int $placeId, ?int $limit = null, ?int $offset = null) : ProductModelCollection
    {
        $productModelCollection = new ProductModelCollection();
        $sql = 'SELECT
                id,
                place_id,
                name,
                price,
                created_at,
                created_by
            FROM products 
            WHERE place_id = :placeId
            ORDER BY id DESC
        ';
        if ($limit) {
            $sql .= ' LIMIT ' . $limit;
        }
        if ($offset) {
            $sql .= ' OFFSET ' . $offset;
        }
        $statement = $this->db->prepare($sql);
        $result = $statement->execute([
            'placeId' => $placeId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from products');
        }
        $result = $statement->fetchAll();

        return $productModelCollection->buildFromArray($result);
    }

    /**
     * @param int $placeId
     *
     * @return int
     * @throws \Exception
     */
    public function countProductsByPlaceId(int $placeId) : int
    {
        $statement = $this->db->prepare('SELECT
                count(1)
            FROM products
            where place_id = :placeId;
        ');
        $result = $statement->execute([
            'placeId' => $placeId,
        ]);
        if (!$result) {
            throw new \Exception('Cant get data from products');
        }

        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    /**
     * @return ProductModelCollection
     * @throws \ReflectionException
     */
    public function getBestPrice() : ProductModelCollection
    {
        $productModelCollection = new ProductModelCollection();
        $sql = 'SELECT 
                   id,
                   place_id,
                   name,
                   price,
                   created_at,
                   created_by
            FROM products p1
                   JOIN (SELECT MIN(price) as minPrice, name as nameMin from products group by name) as p2
            ON p1.name = p2.nameMin AND p1.price = p2.minPrice;
        ';
        $statement = $this->db->prepare($sql);
        $result = $statement->execute([]);
        if (!$result) {
            throw new \Exception('Cant get data from products');
        }
        $result = $statement->fetchAll();

        return $productModelCollection->buildFromArray($result);
    }
}
