<?php
declare(strict_types=1);
namespace App\Src\Product;

use App\Engine\Exception\ValidationException;

/**
 * Class ProductService
 *
 * @package App\Src\Product
 */
class ProductService
{
    /**
     * @var ProductCommandRepository
     */
    private $productCommandRepository;

    /**
     * @var ProductQueryRepository
     */
    private $productQueryRepository;

    /**
     * ProductService constructor.
     *
     * @param ProductCommandRepository $productCommandRepository
     * @param ProductQueryRepository   $productQueryRepository
     */
    public function __construct(
        ProductCommandRepository $productCommandRepository,
        ProductQueryRepository $productQueryRepository
    ) {
        $this->productCommandRepository = $productCommandRepository;
        $this->productQueryRepository = $productQueryRepository;
    }

    /**
     * @param ProductCreateCommand $productCreateCommand
     *
     * @return int
     * @throws ValidationException
     * @throws \Exception
     */
    public function create(ProductCreateCommand $productCreateCommand) : int
    {
        if (!$productCreateCommand->valid()) {
            throw new ValidationException(
                'Cant create product ', implode(', ', $productCreateCommand->getErrors())
            );
        }

        return $this->productCommandRepository->create($productCreateCommand);
    }

    /**
     * @param int      $placeId
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return ProductModelCollection
     * @throws \ReflectionException
     */
    public function getByPlaceId(int $placeId, ?int $limit = null, ?int $offset = null) : ProductModelCollection
    {
        return $this->productQueryRepository->getByPlaceId($placeId, $limit, $offset);
    }

    /**
     * @return ProductModel|null
     * @throws \Exception
     */
    public function getLast() : ?ProductModel
    {
        return $this->productQueryRepository->getLast();
    }

    /**
     * @param int $placeId
     *
     * @return int
     * @throws \Exception
     */
    public function countProductsByPlaceId(int $placeId) : int
    {
        return $this->productQueryRepository->countProductsByPlaceId($placeId);
    }

    /**
     * @param int $placeId
     *
     * @return int
     */
    public function deleteByPlaceId(int $placeId) : int
    {
        return $this->productCommandRepository->deleteByPlaceId($placeId);
    }

    /**
     * @return ProductModelCollection
     * @throws \ReflectionException
     */
    public function getBestPrice() : ProductModelCollection
    {
        return $this->productQueryRepository->getBestPrice();
    }
}
