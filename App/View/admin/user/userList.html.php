<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a class="btn btn-sm btn-outline-secondary" href="<?= $this->generateUrl('adminUsersAdd'); ?>">Add</a>
            </div>
        </div>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    /**
     * @var $users \App\Src\User\UserModelCollection
     */
    $users = $this->get('users');
    ?>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">List</h6>
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Email</th>
                <th>User type</th>
                <th>Created at</th>
                <th>Last login</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($users as $user) {
                echo sprintf('<tr><td>%d</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                    $user->getId(),
                    $user->getEmail(),
                    $user->getUserLevelName(),
                    $user->getCreatedAt()->format('Y-m-d H:i:s'),
                    $user->getLastLogin() ? $user->getLastLogin()->format('Y-m-d H:i:s') : 'None'
                );
            }
            ?>
            </tbody>
        </table>
    </div>
</main>