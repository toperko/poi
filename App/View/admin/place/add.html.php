<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    ?>
    <form action="<?= $this->generateUrl('placeAddNew') ?>" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="inputCategory" class="col-sm-2 col-form-label">Category</label>
            <div class="col-sm-10">
                <select id="inputCategory" class="form-control" name="categoryId">
                    <option value="0"></option>
                    <?php
                    /**
                     * @var $categories \App\Src\Category\CategoryModelCollection
                     */
                    $categories = $this->get('categories');
                    foreach ($categories as $key => $category) {
                        echo '<option 
                                value="' . $category->getId() . '" 
                                ' . (($this->get('categoryId') == $category->getId()) ? 'selected' : '') . '
                               >' . $category->getName() . '</option>';
                    }
                    ?>

                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPlaceName" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input
                        id="inputPlaceName"
                        type="text"
                        class="form-control"
                        name="placeName"
                        placeholder="Name"
                    <?= $this->get('placeName') ? 'value="' . $this->get('placeName') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPlaceDescription" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <textarea
                        id="inputPlaceDescription"
                        class="form-control"
                        name="placeDescription"
                ><?= $this->get('placeDescription') ? $this->get('placeDescription') : '' ?></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEventName" class="col-sm-2 col-form-label">File (optional jpg/png)</label>
            <div class="col-sm-10">
                <div class="form-group">
                    <input type="file" name="img" class="file" accept=".jpg, .png">
                    <div class="input-group col-xs-12">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                        <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                        <span class="input-group-btn">
                            <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPlaceAddress" class="col-sm-2 col-form-label">Address</label>
            <div class="col-sm-10">
                <input
                        id="inputPlaceAddress"
                        type="text"
                        class="form-control"
                        name="placeAddress"
                    <?= $this->get('placeAddress') ? 'value="' . $this->get('placeAddress') . '"' : '' ?>
                        readonly
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPlaceLatitude" class="col-sm-2 col-form-label">Latitude</label>
            <div class="col-sm-10">
                <input
                        id="inputPlaceLatitude"
                        type="text"
                        class="form-control"
                        name="placeLatitude"
                    <?= $this->get('placeLatitude') ? 'value="' . $this->get('placeLatitude') . '"' : '' ?>
                        readonly
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPlaceLongitude" class="col-sm-2 col-form-label">Longitude</label>
            <div class="col-sm-10">
                <input
                        id="inputPlaceLongitude"
                        type="text"
                        class="form-control"
                        name="placeLongitude"
                    <?= $this->get('placeLongitude') ? 'value="' . $this->get('placeLongitude') . '"' : '' ?>
                        readonly
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPlaceDescription" class="col-sm-2 col-form-label">Location</label>
            <div class="col-sm-10">
                <div class="mapInput" id="mapInput"></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </div>
    </form>
</main>