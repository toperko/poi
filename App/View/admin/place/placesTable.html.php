<div class="my-3 p-3 box rounded box-shadow">
    <h6 class="border-bottom border-gray pb-2 mb-0">Places</h6>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        /**
         * @var $places \App\Src\Place\PlaceModelCollection
         * @var $this  \App\Engine\View
         */
        $places = $this->get('places');;
        foreach ($places as $key => $place) :
            ?>
            <tr>
                <th scope="row"><?= $key ?></th>
                <td><?= $place->getName() ?></td>
                <td>
                    <a href="<?= $this->generateUrl('placeDetails', ['id' => $place->getId()]) ?>" class="btn btn-primary">Details</a>
                </td>
                <td>
                    <a href="<?= $this->generateUrl('placeDelete', ['id' => $place->getId()]) ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
        </tbody>
    </table>
</div>