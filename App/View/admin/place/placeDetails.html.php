<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="<?= $this->generateUrl('categoryDetails', ['id' => $this->get('category')->getId()]) ?>"
                   class="btn btn-sm btn-outline-secondary">Back</a>
            </div>

        </div>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    /**
     * @var $category   \App\Src\Category\CategoryModel
     * @var $place      \App\Src\Place\PlaceModel
     * @var $user       \App\Src\User\UserModel
     * @var $createdBy  \App\Src\User\UserModel
     */
    $category = $this->get('category');
    $place = $this->get('place');
    $user = $this->get('user');
    $createdBy = $this->get('createdBy');
    ?>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Details</h6>
        <table class="table table-striped">
            <tbody>
            <tr>
                <th>Place name:</th>
                <td><?= $place->getName(); ?></td>
            </tr>
            <tr>
                <th>Category:</th>
                <td><?= $category->getName(); ?></td>
            </tr>
            <tr>
                <th>Description:</th>
                <td><?= $place->getDescription(); ?></td>
            </tr>
            <tr>
                <th>Address:</th>
                <td><?= $place->getAddress(); ?></td>
            </tr>
            <tr>
                <th>Latitude:</th>
                <td><?= $place->getLatitude(); ?></td>
            </tr>
            <tr>
                <th>Longitude:</th>
                <td><?= $place->getLongitude(); ?></td>
            </tr>
            <tr>
                <th>Created at:</th>
                <td><?= $place->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
            </tr>
            <tr>
                <th>Created by:</th>
                <td><?= $createdBy->getEmail(); ?></td>
            </tr>
            <tr>
                <th>Deleted at:</th>
                <td><?= $place->getDeletedAt() ? $place->getDeletedAt()->format('Y-m-d H:i:s') : '' ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="my-3 p-3 box rounded box-shadow">
        <a href="<?= $this->generateUrl('addProductForPlace', ['placeId' => $place->getId()]) ?>"
           class="btn btn-primary">Add new Product</a>
    </div>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Location</h6>
        <div class="mapInput" id="mapPreview" lat="<?= $place->getLatitude(); ?>"
             lng="<?= $place->getLongitude(); ?>"></div>
    </div>
    <?php if ($this->get('imgUrl')) : ?>
        <div class="my-3 p-3 box rounded box-shadow">
            <h6 class="border-bottom border-gray pb-2 mb-0">Image</h6>
            <img src="<?= $this->get('imgUrl'); ?>" class="img-thumbnail">
        </div>
    <?php endif; ?>
    <div class="my-3 p-3 box rounded box-shadow" id="products">
        <h6 class="border-bottom border-gray pb-2 mb-0">Products</h6>
        <img class="img-center" src="<?= $this->getAssetsUrl('img/ajax-loader.gif'); ?>">
    </div>

</main>