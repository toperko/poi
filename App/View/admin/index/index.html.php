<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
        if($this->get('lastProduct')):
    ?>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Last added product</h6>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th>Category:</th>
                    <td><?= $this->get('lastPlaceCategory')->getName(); ?></td>
                    <th>Place:</th>
                    <td><?= $this->get('lastProductPlace')->getName(); ?></td>
                    <th>Time:</th>
                    <td><?= $this->get('lastProduct')->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
                </tr>
                <tr>
                    <th>Product Name:</th>
                    <td><?= $this->get('lastProduct')->getName(); ?></td>
                    <th>Price:</th>
                    <td colspan="3"><?= $this->get('lastProduct')->getPriceAsString(); ?></td>
                </tr>
                <tr>
                    <th>Address:</th>
                    <td><?= $this->get('lastProductPlace')->getAddress(); ?></td>
                    <th>Latitude:</th>
                    <td><?= $this->get('lastProductPlace')->getLatitude(); ?></td>
                    <th>Longitude:</th>
                    <td><?= $this->get('lastProductPlace')->getLongitude(); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php
    endif;
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    $this->renderHTML('categoriesTable', 'category/', false);
    ?>
</main>