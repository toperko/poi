<div class="my-3 p-3 box rounded box-shadow">
    <h6 class="border-bottom border-gray pb-2 mb-0">Categories</h6>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Color</th>
            <th scope="col">Created at</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        /**
         * @var $categories \App\Src\Category\CategoryModelCollection
         * @var $this  \App\Engine\View
         */
        $categories = $this->get('categories');
        foreach ($categories as $key => $category) :
            ?>
            <tr>
                <th scope="row"><?= $key ?></th>
                <td><?= $category->getName() ?></td>
                <td style="color: <?= $category->getMarkerColor() ?>;"><?= $category->getMarkerColor() ?></td>
                <td><?= $category->getCreatedAt()->format('Y-m-d H:i:s') ?></td>
                <td>
                    <a href="<?= $this->generateUrl('categoryDetails', ['id' => $category->getId()]) ?>" class="btn btn-info">Details</a>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
        </tbody>
    </table>
</div>