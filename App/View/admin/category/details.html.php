<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    /**
     * @var $category \App\Src\Category\CategoryModel
     * @var $user \App\Src\User\UserModel
     */
    $category = $this->get('category');
    $createdBy = $this->get('createdBy');
    ?>
    <div class="my-3 p-3 box rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Details</h6>
        <table class="table table-striped">
            <tbody>
            <tr>
                <th>Name:</th>
                <td><?= $category->getName(); ?></td>
            </tr>
            <tr>
                <th>Description:</th>
                <td><?= $category->getDescription(); ?></td>
            </tr>
            <tr>
                <th>Created by:</th>
                <td><?= $createdBy->getEmail(); ?></td>
            </tr>
            <tr>
                <th>Created at:</th>
                <td><?= $category->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
            </tr>
            <tr>
                <th>Deleted at:</th>
                <td><?= $category->getDeletedAt() ? $category->getDeletedAt()->format('Y-m-d H:i:s') : '' ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="my-3 p-3 box rounded box-shadow">
        <a href="<?= $this->generateUrl('placeAddNewForCategory', ['categoryId' => $category->getId()]) ?>"
           class="btn btn-primary">Add new Place</a>
    </div>
    <?php
    $this->renderHTML('placesTable', 'place/', false);
    ?>
</main>