<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    ?>
    <form action="<?= $this->generateUrl('categoryAddNew') ?>" method="post">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input
                        type="text"
                        class="form-control"
                        name="categoryName"
                        placeholder="Name"
                    <?= $this->get('categoryName') ? 'value="' . $this->get('categoryName') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputCategoryDescription" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <textarea
                        id="inputCategoryDescription"
                        class="form-control"
                        name="categoryDescription"
                ><?= $this->get('categoryDescription') ? $this->get('categoryDescription') : '' ?></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputCategoryMarkerColor" class="col-sm-2 col-form-label">Color</label>
            <div class="col-sm-10">
                <select id="inputCategoryMarkerColor" class="form-control" name="categoryMarkerColor">
                    <?php
                    foreach (\App\Src\Category\CategoryConst::MARKER_COLORS as $key => $color) {
                        echo '<option value="' . $color . '" 
                                ' . (($this->get('categoryMarkerColor') == $color) ? 'selected' : '') . '
                               >' . $color . '</option>';
                    }
                    ?>

                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </div>
    </form>
</main>