<!DOCTYPE html>
<html lang="<?= $this->translation->getLang(); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $this->settings->applicationName ?><?= (isset($this->pageTitle)) ? ' | ' . $this->pageTitle : '' ?></title>
    <link rel="icon" href="<?= $this->settings->assets->favicon ?>">

    <?= $this->generateCssLinks() ?>
</head>
