<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?= $this->get('pageTitle'); ?></h1>
    </div>
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    ?>
    <form action="<?= $this->generateUrl('addProduct') ?>" method="post">
        <div class="form-group row">
            <label for="inputPlace" class="col-sm-2 col-form-label">Place</label>
            <div class="col-sm-10">
                <select id="inputPlace" class="form-control" name="placeId">
                    <option value="0"></option>
                    <?php
                    /**
                     * @var $places \App\Src\Place\PlaceModelCollection
                     */
                    $places = $this->get('places');
                    foreach ($places as $key => $place) {
                        echo '<option 
                                value="' . $place->getId() . '" 
                                ' . (($this->get('placeId') == $place->getId()) ? 'selected' : '') . '
                               >' . $place->getName() . '</option>';
                    }
                    ?>

                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputProductName" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input
                    id="inputProductName"
                    type="text"
                    class="form-control"
                    name="productName"
                    placeholder="Name"
                    <?= $this->get('productName') ? 'value="' . $this->get('productName') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputProductPrice" class="col-sm-2 col-form-label">Price</label>
            <div class="col-sm-10">
                <input
                    id="inputProductPrice"
                    type="number"
                    step="0.01"
                    class="form-control"
                    name="productPrice"
                    placeholder="0"
                    <?= $this->get('productPrice') ? 'value="' . $this->get('productPrice') . '"' : '' ?>
                >
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </div>
    </form>
</main>