<?php
/**
 * @var $this  \App\Engine\View
 * @var $place \App\Src\Place\PlaceModel
 */
$place = $this->get('place');
?>
<div class="jumbotron" id="map" lat="<?= $place->getLatitude(); ?>" lng="<?= $place->getLongitude(); ?>"></div>
<main role="main" class="container">
    <div class="row">
        <div class="col-md-12">
            <h2><?= $this->get('pageTitle'); ?> <a href="<?= $this->generateUrl('addPlaceToRoute', ['placeId' => $place->getId()]) ?>" class="badge badge-success">Add to route</a></h2>
            <p><?= $this->get('category')->getDescription(); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th>Category:</th>
                    <td><?= $this->get('category')->getName(); ?></td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td><?= $place->getName(); ?></td>
                </tr>
                <tr>
                    <th>Description:</th>
                    <td><p><?= $place->getDescription(); ?></p></td>
                </tr>
                <tr>
                    <th>Address:</th>
                    <td><?= $place->getAddress(); ?></td>
                </tr>
                <tr>
                    <th>Latitude:</th>
                    <td><?= $place->getLatitude(); ?></td>
                </tr>
                <tr>
                    <th>Longitude:</th>
                    <td><?= $place->getLongitude(); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <?php
        if ($this->get('imgUrl')) :
            ?>
            <div class="col-md-6">
                <img class="img-fluid" src="<?= $this->get('imgUrl'); ?>" alt="<?= $place->getName(); ?>">
            </div>
        <?php
        endif;
        ?>
    </div>
    <div class="row rounded">
        <h5>Products</h5>
        <hr>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Created at</th>
            </tr>
            </thead>
            <tbody>


            <?php
            /**
             * @var $products \App\Src\Product\ProductModelCollection
             */
            $products = $this->get('products');
            foreach ($products as $product) :
                ?>
                <tr>
                    <td><?= $product->getName(); ?></td>
                    <td><?= $product->getPriceAsString(); ?></td>
                    <td><?= $product->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
                </tr>
            <?php
            endforeach;
            ?>
            </tbody>
        </table>
    </div>
    <hr>
</main>


