<div class="container" style="padding-top: 20px;">
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    $this->session->removeAllFlash();
    if ($this->routeService->getRoute()->isEmpty()) {
        echo '<h2>' . $this->get('pageTitle') . '</h2>';
    } else {
        echo '<h2>' . $this->get('pageTitle') . ' <a href="' . $this->generateUrl('removeRoute') . '" class="badge badge-danger">Remove all</a></h2>';
    }
    ?>
    <hr>
    <button class="btn btn-light" id="sort-distance">Sort by distance</button>
    <button class="btn btn-light" id="sort-time">Sort by time</button>
    <hr>
    <div id="route-results">
        <?php
        foreach ($this->get('routes') as $key => $route) {
            echo '<div class="jumbotron route-result" id="route-result-' . $key . '"><p>';
            foreach ($route as $keyPlace => $place) {
                echo '<a href="' . (($keyPlace == 0 || $keyPlace == count($route) - 1) ? $this->generateUrl('setYourLocalization') : $this->generateUrl('placeById', ['placeId' => $place['id']])) . '" class="btn btn-' . (($keyPlace == 0 || $keyPlace == count($route) - 1) ? 'primary' : 'secondary') . '">' . $place['street'] . '</a>';
                if ($keyPlace != count($route) - 1) {
                    echo '<span data-feather="arrow-right"></span>';
                }
            }
            echo '</p>';
            echo '<p>Distance (km): <b id="distance-' . $key . '">0</b> Time: <b id="duration-' . $key . '">0</b></p>';
            echo '<div id="route-map-' . $key . '" class="map"></div></div>';
        }
        ?>
    </div>
    <hr>
</div>

