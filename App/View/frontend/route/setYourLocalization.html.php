<div class="container" style="padding-top: 20px;">
    <?php
    /**
     * @var $this \App\Engine\View
     */
    if ($this->routeService->getRoute()->isEmpty()) {
        echo '<h2>' . $this->get('pageTitle') . '</h2>';
    } else {
        echo '<h2>' . $this->get('pageTitle') . ' <a href="' . $this->generateUrl('removeRoute') . '" class="badge badge-danger">Remove</a></h2>';
    }
    if ($this->session->hasFlash()) {
        $this->renderFlashMessages($this->session->getFlashCollection());
        $this->session->removeAllFlash();
    }
    $this->session->removeAllFlash();
    ?>
    <hr>
    <form action="<?= $this->generateUrl('setYourLocalization') ?>" method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Location</label>
            <div class="col-sm-10">
                <div id="setYourLocalization" class="map"></div>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputAddress" class="col-sm-2 col-form-label">Address</label>
            <div class="col-sm-10">
                <input
                        id="inputAddress"
                        type="text"
                        class="form-control"
                        name="address"
                    <?= $this->get('address') ? 'value="' . $this->get('address') . '"' : '' ?>
                        readonly
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputLatitude" class="col-sm-2 col-form-label">Latitude</label>
            <div class="col-sm-10">
                <input
                        id="inputLatitude"
                        type="text"
                        class="form-control"
                        name="latitude"
                    <?= $this->get('latitude') ? 'value="' . $this->get('latitude') . '"' : '' ?>
                        readonly
                >
            </div>
        </div>
        <div class="form-group row">
            <label for="inputLongitude" class="col-sm-2 col-form-label">Longitude</label>
            <div class="col-sm-10">
                <input
                        id="inputLongitude"
                        type="text"
                        class="form-control"
                        name="longitude"
                    <?= $this->get('longitude') ? 'value="' . $this->get('longitude') . '"' : '' ?>
                        readonly
                >
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-primary">Set</button>
            </div>
        </div>
    </form>
    <hr>
</div>

