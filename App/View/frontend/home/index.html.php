<div class="jumbotron" id="map"></div>

<main role="main" class="container">
    <div class="row" id="places-list">

    </div>
    <div class="categories row">
        <?php
        /**
         * @var $this       \App\Engine\View
         * @var $categories \App\Src\Category\CategoryModelCollection
         */
        if ($this->session->hasFlash()) {
            echo '<div class="col-md-12">';
            $this->renderFlashMessages($this->session->getFlashCollection());
            $this->session->removeAllFlash();
            echo '</div>';
        }
        $categories = $this->get('categories');
        foreach ($categories as $category) :
            ?>
            <div class="col-md-4">
                <h2 style="color: <?= $category->getMarkerColor(); ?>;"><?= $category->getName(); ?></h2>
                <p><?= $category->getDescription() ?></p>
                <p>
                    <a class="btn btn-primary"
                       href="<?= $this->generateUrl('categoryById', ['categoryId' => $category->getId()]); ?>"
                    >View details &raquo;</a>
                </p>
            </div>
        <?php endforeach; ?>
    </div>
    <hr>
</main>


