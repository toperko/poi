</main>
<footer class="container">
    <p>&copy; <?= date('Y'); ?></p>
</footer>
<?= $this->generateJsLinks(); ?>
<script>
    feather.replace()
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrTXkQGFdBrpmECCIaEkiyaLCbX94e4v0&callback=initMap">
</script>
</body>
</html>
