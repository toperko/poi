<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="<?= $this->generateUrl('homePage'); ?>">Poi</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse w-100 order-1 order-md-0 dual-collapse2" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('homePage'); ?>">Home</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Categories</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <?php
                    /**
                     * @var $this       \App\Engine\View
                     * @var $categories \App\Src\Category\CategoryModelCollection
                     */
                    $categories = $this->get('categories');
                    foreach ($categories as $category) {
                        echo '<a 
                        class="dropdown-item" 
                        href="' . $this->generateUrl('categoryById', ['categoryId' => $category->getId()]) . '"
                        >' . $category->getName() . '</a>';
                    }
                    ?>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('bestPrice'); ?>">Best price</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->generateUrl('viewRoute'); ?>">Your route</a>
            </li>
        </ul>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <?php
                if ($this->routeService->getRoute()->isEmpty()) {
                    echo '<a 
                                class="btn btn-outline-primary" 
                                href="' . $this->generateUrl('setYourLocalization') . '"
                                >Set your localization</a>';
                } else {
                    $yourLocalization = $this->routeService->getRoute()->getFirst();
                    echo '<a 
                                class="btn btn-outline-primary" 
                                href="' . $this->generateUrl('setYourLocalization') . '"
                                >' . $yourLocalization->getAddress() . '</a>';
                }
                ?>

            </li>
        </ul>
        <?php
        if ($this->get('user')) {
            echo '<a class="btn btn-outline-success my-2 my-sm-0" href="' . $this->generateUrl('adminHomePage') . '">Panel</a>';
        } else {
            echo '<a class="btn btn-outline-success my-2 my-sm-0" href="' . $this->generateUrl('login') . '">Log in</a>';
        }
        ?>
    </div>
</nav>