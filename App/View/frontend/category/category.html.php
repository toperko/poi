<div class="jumbotron" id="map" categoryId="<?= $this->get('category')->getId(); ?>"></div>
<div class="container">
    <div class="row" id="places-list">

    </div>
    <div class="row categories">
        <div class="col-md-12">
            <h2><?= $this->get('pageTitle'); ?></h2>
            <p><?= $this->get('category')->getDescription(); ?></p>
        </div>
    </div>
    <hr>
</div>


