<?php
declare(strict_types=1);
namespace App\Controller\Frontend;

use App\Engine\App;
use App\Engine\FrontendController;
use App\Src\Category\CategoryService;
use App\Src\Place\PlaceService;

/**
 * Class Category
 *
 * @package App\Controller\Frontend
 */
class Category extends FrontendController
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var PlaceService
     */
    private $placeService;

    /**
     * Category constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->categoryService = $app->getContainer()->get('categoryService');
        $this->placeService = $app->getContainer()->get('placeService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function getById()
    {
        $categoryId = $this->get('categoryId', FILTER_VALIDATE_INT);
        $category = $this->categoryService->getById($categoryId);
        if (!$category) {
            $this->app->redirect($this->generateUrl('404'));
        }
        $places = $this->placeService->getByCategoryId($category->getId());
        $view = $this->getView();
        $view->addJs('categoryPage', 'categoryPage.js');
        $view->set('pageTitle', 'Category ' . $category->getName());
        $view->set('category', $category);
        $view->set('categories', $this->categoryService->getAll());
        $view->set('places', $places);
        $view->renderHTML('category', 'category/');
    }
}
