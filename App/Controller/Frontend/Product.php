<?php
declare(strict_types=1);
namespace App\Controller\Frontend;

use App\Engine\App;
use App\Engine\FrontendController;
use App\Src\Category\CategoryService;
use App\Src\Place\PlaceService;
use App\Src\Product\ProductService;

/**
 * Class Product
 *
 * @package App\Controller\Frontend
 */
class Product extends FrontendController
{
    /**
     * @var PlaceService
     */
    private $placeService;

    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * Place constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->placeService = $app->getContainer()->get('placeService');
        $this->categoryService = $app->getContainer()->get('categoryService');
        $this->productService = $app->getContainer()->get('productService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function bestPrice()
    {
        $view = $this->getView();
        $view->addJs('bestProductPage', 'bestProductPage.js');
        $view->set('pageTitle', 'Best Price');
        $view->set('categories', $this->categoryService->getAll());
        $view->renderHTML('best', 'product/');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function bestPriceAjax()
    {
        $categories = $this->categoryService->getAll();
        $products = $this->productService->getBestPrice()->toArray();
        foreach ($products as &$product){
            $product['location'] = $this->placeService->getById($product['placeId'])->toArray();
            $product['location']['color'] = $categories->getByKey($product['location']['categoryId'])->getMarkerColor();
        }
        $this->getView()->renderJSON($products);
    }
}
