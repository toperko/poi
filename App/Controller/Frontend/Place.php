<?php
declare(strict_types=1);
namespace App\Controller\Frontend;

use App\Engine\App;
use App\Engine\FrontendController;
use App\Src\Category\CategoryService;
use App\Src\Place\PlaceService;
use App\Src\Product\ProductService;

/**
 * Class Places
 *
 * @package App\Controller\Frontend
 */
class Place extends FrontendController
{
    /**
     * @var PlaceService
     */
    private $placeService;

    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * Place constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->placeService = $app->getContainer()->get('placeService');
        $this->categoryService = $app->getContainer()->get('categoryService');
        $this->productService = $app->getContainer()->get('productService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function getAllAjax()
    {
        $categories = $this->categoryService->getAll();
        $places = $this->placeService->getAll();
        $places = $places->toArray();
        foreach ($places as &$place) {
            $place['color'] = $categories->getByKey($place['categoryId'])->getMarkerColor();
        }
        $this->getView()->renderJSON($places);
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function getByCategoryAjax()
    {
        $categoryId = $this->get('categoryId', FILTER_VALIDATE_INT);
        $category = $this->categoryService->getById($categoryId);
        if (!$category) {
            http_response_code(404);
            exit;
        }
        $places = $this->placeService->getByCategoryId($category->getId());
        $places = $places->toArray();
        foreach ($places as &$place) {
            $place['color'] = $category->getMarkerColor();
        }
        $this->getView()->renderJSON($places);
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function getById()
    {
        $placeId = $this->get('placeId', FILTER_VALIDATE_INT);
        $place = $this->placeService->getById($placeId);
        if (!$place) {
            $this->app->redirect($this->generateUrl('404'));
        }
        $products = $this->productService->getByPlaceId($place->getId());
        $category = $this->categoryService->getById($place->getCategoryId());
        $view = $this->getView();
        $view->addJs('placePage', 'placePage.js');
        $view->set('categories', $this->categoryService->getAll());
        $view->set('pageTitle', $place->getName());
        $view->set('category', $category);
        $view->set('products', $products);
        $view->set('place', $place);
        $view->set('createdBy', $this->userService->getUserById($place->getCreatedBy()));
        if ($place->getImg()) {
            $view->set('imgUrl', $this->generateUrl('upload/' . $place->getImg()));
        }
        $view->renderHTML('place', 'place/');
    }
}
