<?php
declare(strict_types=1);
namespace App\Controller\Frontend;

use App\Engine\App;
use App\Engine\FrontendController;
use App\Engine\Session\FlashMessage;
use App\Helper;
use App\Src\Category\CategoryService;
use App\Src\Localization\LocalizationModel;
use App\Src\Place\PlaceService;
use App\Src\Route\RouteService;

/**
 * Class Route
 *
 * @package App\Controller\Frontend
 */
class Route extends FrontendController
{
    /**
     * @var RouteService
     */
    protected $routeService;

    /**
     * @var PlaceService
     */
    protected $placeService;

    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * Route constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->routeService = $app->getContainer()->get('routeService');
        $this->placeService = $app->getContainer()->get('placeService');
        $this->categoryService = $app->getContainer()->get('categoryService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function setYourLocalization()
    {
        $view = $this->getView();
        $view->addJs('setYourLocalization', 'setYourLocalization.js');
        $view->set('pageTitle', 'Set your localization');
        $view->set('categories', $this->categoryService->getAll());
        $view->set('address', $this->post('address', FILTER_SANITIZE_STRING));
        $view->set('latitude', $this->post('latitude', FILTER_SANITIZE_STRING));
        $view->set('longitude', $this->post('longitude', FILTER_SANITIZE_STRING));
        if ($this->isGet() && !$this->routeService->getRoute()->isEmpty()) {
            $route = $this->routeService->getRoute()->getFirst();
            $view->set('address', $route->getAddress());
            $view->set('latitude', $route->getLatitude());
            $view->set('longitude', $route->getLongitude());
        }
        if ($this->isGet()) {
            $view->renderHTML('setYourLocalization', 'route/');

            return;
        }
        try {
            $location = (new LocalizationModel())
                ->setAddress($this->post('address', FILTER_SANITIZE_STRING))
                ->setLatitude($this->post('latitude', FILTER_SANITIZE_STRING))
                ->setLongitude($this->post('longitude', FILTER_SANITIZE_STRING));
            if ($this->routeService->getRoute()->isEmpty()) {
                $this->routeService->addToRoute($location);
            } else {
                $route = $this->routeService->getRoute();
                $route->getFirst()
                    ->setAddress($location->getAddress())
                    ->setLatitude($location->getLatitude())
                    ->setLongitude($location->getLongitude());
                $this->routeService->setRoute($route);
            }
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setTitle('Your location set')
                    ->setMessage($location->getAddress())
            );
            $this->app->redirect($this->generateUrl('homePage'));
        } catch (\Throwable $exception) {
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        }
        $view->renderHTML('setYourLocalization', 'route/');
    }

    /**
     * @throws \Exception
     */
    public function addToRoute()
    {
        if ($this->routeService->getRoute()->isEmpty()) {
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::INFO_TYPE)
                    ->setMessage('First set your localization.')
            );
            $this->app->redirect($this->generateUrl('setYourLocalization'));
        }
        if ($this->routeService->getRoute()->count() >= 5) {
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage('You can add max 4 location')
            );
            $this->app->redirect($this->generateUrl('homePage'));
        }
        $placeId = $this->get('placeId', FILTER_VALIDATE_INT);
        $place = $this->placeService->getById($placeId);
        if (!$place) {
            $this->app->redirect($this->generateUrl('404'));
        }
        foreach ($this->routeService->getRoute() as $localizationModel) {
            if ($localizationModel->getLongitude() === $place->getLongitude() && $localizationModel->getLatitude() === $place->getLatitude()) {
                $this->app->getSession()->addFlash(
                    (new FlashMessage())
                        ->setType(FlashMessage::DANGERS_TYPE)
                        ->setMessage('You can not add the same point.')
                );
                $this->app->redirect($this->generateUrl('homePage'));
            }
        }
        $this->routeService->addToRoute($place);
        $this->app->getSession()->addFlash(
            (new FlashMessage())
                ->setType(FlashMessage::SUCCESS_TYPE)
                ->setTitle('Location Added: ')
                ->setMessage($place->getAddress())
        );
        $this->app->redirect($this->generateUrl('homePage'));
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function ajaxGetRoute()
    {
        $this->getView()->renderJSON($this->getRoute());
    }

    /**
     * @return array
     */
    protected function getRoute() : array
    {
        $places = [];
        $yourLocation = [];
        $route = $this->routeService->getRoute();
        foreach ($route as $key => $place) {
            if ($key == 0) {
                $yourLocation = [
                    'street'    => explode(',', $place->getAddress())[0],
                    'address'   => $place->getAddress(),
                    'latitude'  => $place->getLatitude(),
                    'longitude' => $place->getLongitude(),
                ];
                continue;
            }
            $places[] = [
                'id'        => $place->getId(),
                'street'    => explode(',', $place->getAddress())[0],
                'address'   => $place->getAddress(),
                'latitude'  => $place->getLatitude(),
                'longitude' => $place->getLongitude(),
            ];
        }
        $placesRoutes = Helper::permutations($places);
        if (count($placesRoutes) > 1) {
            $placesRoutes = array_slice($placesRoutes, 0, count($placesRoutes) / 2);
        }
        foreach ($placesRoutes as &$placeRoute) {
            array_unshift($placeRoute, $yourLocation);
            array_push($placeRoute, $yourLocation);
        }

        return $placesRoutes;
    }

    /**
     * Remove your localization and route;
     *
     * @throws \ReflectionException
     */
    public function removeYourLocalization()
    {
        $this->routeService->reset();
        $this->app->getSession()->addFlash(
            (new FlashMessage())
                ->setType(FlashMessage::INFO_TYPE)
                ->setTitle('Route deleted')
                ->setMessage('')
        );
        $this->app->redirect($this->generateUrl('homePage'));
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function viewRoute()
    {
        $route = $this->getRoute();
        if (empty($route)) {
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage('Your route is empty.')
            );
            $this->app->redirect($this->generateUrl('homePage'));
        }
        $view = $this->getView();
        $view->addJs('viewRoutePage', 'viewRoutePage.js');
        $view->addJs('moment', 'moment.min.js');
        $view->set('categories', $this->categoryService->getAll());
        $view->set('pageTitle', 'Your routes:');
        $view->set('routes', $route);
        $view->renderHTML('viewRoute', 'route/');
    }
}
