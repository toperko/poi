<?php
declare(strict_types=1);
namespace App\Controller\Frontend;

use App\Engine\App;
use App\Engine\FrontendController;
use App\Src\Category\CategoryService;

/**
 * Class HomePage
 *
 * @package App\Controller\Frontend
 */
class HomePage extends FrontendController
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * HomePage constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->categoryService = $app->getContainer()->get('categoryService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function index()
    {
        $view = $this->getView();
        $view->addJs('homePage', 'homePage.js');
        $view->set('categories', $this->categoryService->getAll());
        $view->renderHTML('index', 'home/', true);
    }
}
