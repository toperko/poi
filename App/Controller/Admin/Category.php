<?php
declare(strict_types=1);
namespace App\Controller\Admin;

use App\Engine\AdminController;
use App\Engine\App;
use App\Engine\Session\FlashMessage;
use App\Src\Place\PlaceService;
use App\Src\Category\CategoryCreateCommand;
use App\Src\Category\CategoryService;
use Exception;
use Throwable;

/**
 * Class Category
 *
 * @package App\Controller\Admin
 */
class Category extends AdminController
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var PlaceService
     */
    private $placeService;

    /**
     * Category constructor.
     *
     * @param App $app
     *
     * @throws Throwable
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->categoryService = $app->getContainer()->get('categoryService');
        $this->placeService = $app->getContainer()->get('placeService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws Throwable
     */
    public function add()
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Add new category');
        $view->set('categoryName', $this->post('categoryName', FILTER_SANITIZE_STRING));
        $view->set('categoryMarkerColor', $this->post('categoryMarkerColor', FILTER_SANITIZE_STRING));
        $view->set('categoryDescription', $this->post('categoryDescription', FILTER_SANITIZE_STRING));
        if ($this->isGet()) {
            $view->renderHTML('add', 'category/');

            return;
        }
        try {
            $this->beginDbTransaction();
            $categoryCreateCommand = (new CategoryCreateCommand())
                ->setName($this->post('categoryName', FILTER_SANITIZE_STRING))
                ->setDescription($this->post('categoryDescription', FILTER_SANITIZE_STRING))
                ->setMarkerColor($this->post('categoryMarkerColor', FILTER_SANITIZE_STRING))
                ->setCreatedBy($this->userService->getUserLogin()->getId());
            if (!$categoryCreateCommand->valid()) {
                foreach ($categoryCreateCommand->getErrors() as $error) {
                    $this->app->getSession()->addFlash(
                        (new FlashMessage())
                            ->setType(FlashMessage::DANGERS_TYPE)
                            ->setMessage($error)
                    );
                }
                $view->renderHTML('add', 'category/');

                return;
            }
            $this->categoryService->create($categoryCreateCommand);
            $this->commitDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Page added')
            );
            $this->app->redirect($this->generateUrl('categoryIndex'));
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        }
        $view->renderHTML('add', 'category/');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function viewCategories()
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Categories');
        $user = $this->userService->getUserLogin();
        $categories = $this->categoryService->getCategoriesCreatedBy($user->getId());
        $view->set('categories', $categories);
        $view->renderHTML('index', 'category/');
    }


    /**
     * @throws \ReflectionException
     */
    public function showDetails()
    {
        try {
            $user = $this->userService->getUserLogin();
            $categoryId = $this->get('id', FILTER_VALIDATE_INT);
            $category = $this->categoryService->getById($categoryId);
            if (!$category) {
                throw new Exception('Cant find this category');
            }
            if ($category->getCreatedBy() !== $user->getId()) {
                throw new Exception('Only owner see category details');
            }
            $view = $this->getView();
            $view->set('category', $category);
            $view->set(
                'places',
                $this->placeService->getByCategoryId($category->getId())
            );
            $view->set(
                'createdBy',
                $this->userService->getUserById($category->getCreatedBy())
            );
            $view->set('pageTitle', 'Category: ' . $category->getName());
            $view->renderHTML('details', 'category/');
        } catch (Throwable $exception) {
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
            $this->app->redirect($this->generateUrl('categoryIndex'));
        }
    }
}
