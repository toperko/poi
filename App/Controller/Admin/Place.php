<?php
declare(strict_types=1);
namespace App\Controller\Admin;

use App\Engine\AdminController;
use App\Engine\App;
use App\Engine\Session\FlashMessage;
use App\Helper;
use App\Src\Place\PlaceCreateCommand;
use App\Src\Place\PlaceModel;
use App\Src\Place\PlaceService;
use App\Src\Product\ProductService;
use App\Src\Category\CategoryService;
use Exception;
use Throwable;
use function pathinfo;
use function move_uploaded_file;
use function file_exists;

/**
 * Class Place
 *
 * @package App\Controller\Admin
 */
class Place extends AdminController
{
    /**
     * @var PlaceService
     */
    private $placeService;

    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @const int
     */
    private const FILE_MAX_SIZE = 2000000;

    /**
     * @const array
     */
    private const FILE_ACCEPT = ['jpg', 'png'];

    /**
     * Place constructor.
     *
     * @param App $app
     *
     * @throws Throwable
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->categoryService = $app->getContainer()->get('categoryService');
        $this->placeService = $app->getContainer()->get('placeService');
        $this->productService = $app->getContainer()->get('productService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     * @throws Throwable
     */
    public function add()
    {
        $view = $this->getView();
        $view->addJs('mapInput', 'mapInput.js');
        $view->set('pageTitle', 'Add new place');
        $view->set(
            'categories',
            $this->categoryService->getAll()
        );
        if ($this->get('categoryId')) {
            $view->set('categoryId', $this->get('categoryId', FILTER_VALIDATE_INT));
        }
        if ($this->isPost()) {
            $view->set('categoryId', $this->post('categoryId', FILTER_VALIDATE_INT));
        }
        $view->set('placeName', $this->post('placeName', FILTER_SANITIZE_STRING));
        $view->set('placeDescription', $this->post('placeDescription', FILTER_SANITIZE_STRING));
        $view->set('placeAddress', $this->post('placeAddress', FILTER_SANITIZE_STRING));
        $view->set('placeLatitude', $this->post('placeLatitude', FILTER_SANITIZE_STRING));
        $view->set('placeLongitude', $this->post('placeLongitude', FILTER_SANITIZE_STRING));
        if ($this->isGet()) {
            $view->renderHTML('add', 'place/');

            return;
        }
        try {
            $this->beginDbTransaction();
            $placeCreateCommand = (new PlaceCreateCommand())
                ->setCategoryId($this->post('categoryId', FILTER_VALIDATE_INT))
                ->setName($this->post('placeName', FILTER_SANITIZE_STRING))
                ->setDescription($this->post('placeDescription', FILTER_SANITIZE_STRING))
                ->setAddress($this->post('placeAddress', FILTER_SANITIZE_STRING))
                ->setLatitude($this->post('placeLatitude', FILTER_SANITIZE_STRING))
                ->setLongitude($this->post('placeLongitude', FILTER_SANITIZE_STRING))
                ->setCreatedBy($this->userService->getUserLoginId());
            if (!$placeCreateCommand->valid()) {
                foreach ($placeCreateCommand->getErrors() as $error) {
                    $this->app->getSession()->addFlash(
                        (new FlashMessage())
                            ->setType(FlashMessage::DANGERS_TYPE)
                            ->setMessage($error)
                    );
                }
                $view->renderHTML('add', 'place/');

                return;
            }
            $createdId = $this->placeService->create($placeCreateCommand);
            if ($file = $this->file('img')) {
                $this->uploadFile($file, $this->placeService->getById($createdId));
            }
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Place added')
            );
            $this->commitDbTransaction();
            $this->app->redirect($this->generateUrl('categoryDetails', ['id' => $placeCreateCommand->getCategoryId()]));
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        }
        $view->renderHTML('add', 'place/');
    }

    /**
     * @param array      $file
     * @param PlaceModel $placeModel
     *
     * @return bool
     * @throws Exception
     */
    private function uploadFile(array $file, PlaceModel $placeModel) : bool
    {
        $fileName = $file['name'];
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $size = $file['size'];
        $tmpName = $file['tmp_name'];
        $hash = Helper::randomHash(10);
        $uploadName = DIR_UPLOAD . '/' . $hash . '.' . $extension;
        $newFileName = $hash . '.' . $extension;
        if ($size > self::FILE_MAX_SIZE) {
            throw new Exception('File is too big');
        }
        if (!in_array($extension, self::FILE_ACCEPT)) {
            throw new Exception('Accept only png files');
        }
        if (move_uploaded_file($tmpName, $uploadName)) {
            $this->placeService->updateImg($placeModel->getId(), $newFileName);
            return true;
        }
        throw new Exception('Cant upload file');
    }

    /**
     * @throws \ReflectionException
     */
    public function deletePlace()
    {
        try {
            $this->beginDbTransaction();
            $placeId = $this->get('id', FILTER_VALIDATE_INT);
            $place = $this->placeService->getById($placeId);
            if (!$place) {
                throw new Exception('Cant find this place');
            }
            $this->placeService->delete($place->getId());
            $this->commitDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Place deleted')
            );
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        } finally {
            if (isset($place)) {
                $this->app->redirect($this->generateUrl('categoryDetails', ['id' => $place->getCategoryId()]));
            }
            $this->app->redirect($this->generateUrl('categoryIndex'));
        }
    }

    /**
     * @throws \ReflectionException
     */
    public function details()
    {
        try {
            $placeId = $this->get('id', FILTER_VALIDATE_INT);
            $place = $this->placeService->getById($placeId);
            if (!$place) {
                throw new Exception('Cant find this place');
            }
            $category = $this->categoryService->getById($place->getCategoryId());
            $view = $this->getView();
            $view->addJs('mapPreview', 'mapPreview.js');
            $view->addJs('placeDetailsScript', 'placeDetailsScript.js');
            $view->set('pageTitle', 'Place: ' . $place->getName());
            $view->set('category', $category);
            $view->set('place', $place);
            $view->set('createdBy', $this->userService->getUserById($place->getCreatedBy()));
            if($place->getImg()){
                $view->set('imgUrl', $this->generateUrl('upload/' . $place->getImg()));
            }

            $view->renderHTML('placeDetails', 'place/');
        } catch (\Throwable $exception) {
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
            if (isset($page)) {
                $this->app->redirect($this->generateUrl('pagesDetails', ['id' => $page->getId()]));
            }
            $this->app->redirect($this->generateUrl('categoryIndex'));
        }
    }
}
