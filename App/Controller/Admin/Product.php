<?php
declare(strict_types=1);
namespace App\Controller\Admin;

use App\Engine\AdminController;
use App\Engine\App;
use App\Engine\Session\FlashMessage;
use App\Src\Place\PlaceService;
use App\Src\Product\ProductCreateCommand;
use App\Src\Product\ProductService;
use App\Src\Category\CategoryService;
use Throwable;
use function http_response_code;
use function ceil;

/**
 * Class Product
 *
 * @package App\Controller\Admin
 */
class Product extends AdminController
{
    /**
     * @var PlaceService
     */
    private $placeService;

    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @const int
     */
    public const PRODUCT_PER_PAGE = 10;

    /**
     * Product constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->placeService = $app->getContainer()->get('placeService');
        $this->categoryService = $app->getContainer()->get('categoryService');
        $this->productService = $app->getContainer()->get('productService');
    }

    /**
     * @throws Throwable
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \ReflectionException
     */
    public function add()
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Add new product');
        $view->set(
            'places',
            $this->placeService->getAll()
        );
        if ($this->get('placeId')) {
            $view->set('placeId', $this->get('placeId', FILTER_VALIDATE_INT));
        }
        if ($this->isPost()) {
            $view->set('placeId', $this->post('placeId', FILTER_VALIDATE_INT));
        }
        $view->set('productName', $this->post('productName', FILTER_SANITIZE_STRING));
        $view->set('productPrice', $this->post('productPrice', FILTER_VALIDATE_FLOAT));
        if ($this->isGet()) {
            $view->renderHTML('add', 'product/');

            return;
        }
        try {
            $this->beginDbTransaction();
            $productCreateCommand = (new ProductCreateCommand())
                ->setPlaceId($this->post('placeId', FILTER_VALIDATE_INT))
                ->setName($this->post('productName', FILTER_SANITIZE_STRING))
                ->setPrice(
                    intval(strval($this->post('productPrice', FILTER_VALIDATE_FLOAT) * 100))
                )
                ->setCreatedBy($this->userService->getUserLoginId());
            if (!$productCreateCommand->valid()) {
                foreach ($productCreateCommand->getErrors() as $error) {
                    $this->app->getSession()->addFlash(
                        (new FlashMessage())
                            ->setType(FlashMessage::DANGERS_TYPE)
                            ->setMessage($error)
                    );
                }
                $view->renderHTML('add', 'product/');

                return;
            }
            $this->productService->create($productCreateCommand);
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::SUCCESS_TYPE)
                    ->setMessage('Product added')
            );
            $this->commitDbTransaction();
            $this->app->redirect($this->generateUrl('placeDetails', ['id' => $productCreateCommand->getPlaceId()]));
        } catch (Throwable $exception) {
            $this->rollbackDbTransaction();
            $this->app->getSession()->addFlash(
                (new FlashMessage())
                    ->setType(FlashMessage::DANGERS_TYPE)
                    ->setMessage($exception->getMessage())
            );
        }
        $view->renderHTML('add', 'product/');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function getProducts()
    {
        $placeId = $this->get('placeId', FILTER_VALIDATE_INT);
        $page = $this->get('page', FILTER_VALIDATE_INT);
        $place = $this->placeService->getById($placeId);
        if (!$place) {
            http_response_code(404);
            $this->getView()->renderJSON(['error' => 'Cant find this place']);
        }
        $offset = ($page - 1) * self::PRODUCT_PER_PAGE;
        $products = $this->productService->getByPlaceId($place->getId(), self::PRODUCT_PER_PAGE, $offset);
        $productsCount = $this->productService->countProductsByPlaceId($place->getId());
        $pageMax = ceil($productsCount / self::PRODUCT_PER_PAGE);
        $this->getView()->renderJSON([
            'count'       => $productsCount,
            'pageCurrent' => $page,
            'pageMax'     => $pageMax,
            'data'        => $products->toArray(),
        ]);
    }
}
