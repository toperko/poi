<?php
declare(strict_types=1);
namespace App\Controller\Admin;

use App\Engine\AdminController;
use App\Engine\App;
use App\Src\Place\PlaceService;
use App\Src\Product\ProductService;
use App\Src\Category\CategoryService;

/**
 * Class Index
 *
 * @package App\Controller\Admin
 */
class Index extends AdminController
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var PlaceService
     */
    private $placeService;

    /**
     * Index constructor.
     *
     * @param App $app
     *
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->categoryService = $app->getContainer()->get('categoryService');
        $this->productService = $app->getContainer()->get('productService');
        $this->placeService = $app->getContainer()->get('placeService');
    }

    /**
     * @throws \App\Engine\Container\ContainerException
     * @throws \App\Engine\Container\ContainerNotFoundException
     * @throws \Throwable
     */
    public function index()
    {
        $view = $this->getView();
        $view->set('pageTitle', 'Dashboard');
        $view->set('categories', $this->categoryService->getAll());
        $view->set('lastProduct', $this->productService->getLast());
        if ($view->get('lastProduct')) {
            $view->set('lastProductPlace', $this->placeService->getById(
                $view->get('lastProduct')->getPlaceId()
            ));
            $view->set('lastPlaceCategory', $this->categoryService->getById(
                $view->get('lastProductPlace')->getCategoryId()
            ));
        }
        $view->renderHTML('index', 'index/');
    }
}
