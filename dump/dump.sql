-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: poi
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `marker_color` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'red',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_users_id_fk` (`created_by`),
  CONSTRAINT `pages_users_id_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (10,'Stacje Paliw','Sprawdź gdzie w pobliżu najlepiej kupić paliwo.','red',1,'2019-01-12 23:57:30',NULL),(11,'Stacje Kontroli Pojazdów','Sprawdź gdzie wykonać okresowe badania techniczne lub sprawdzić stan techniczny pojazdu.','blue',1,'2019-01-12 23:59:00',NULL),(12,'Sklepy Motoryzacynje','Szukasz części? Sprawdź gdzie je kupić i w jakich cenach.','purple',1,'2019-01-12 23:59:58',NULL),(13,'Warsztaty','Problemy z samochodem, sprawdź specjalistów w twojej okolicy.','green',1,'2019-01-13 00:01:06',NULL),(15,'Testowa','Nowa','yellow',1,'2019-01-13 11:36:01',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `events_name_uindex` (`name`),
  KEY `events_pages_id_fk` (`category_id`),
  KEY `events_page_id_name_index` (`category_id`,`name`),
  KEY `events_users_id_fk` (`created_by`),
  CONSTRAINT `events_pages_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `events_users_id_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `places`
--

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
INSERT INTO `places` VALUES (28,10,'Stacja Pod Żaglami Witomino','2019-01-13 00:17:41',1,NULL,'Stacja Pod Żaglami Witomino, otwarte 05:00 - 02:00','yx4sBBl1jo.jpg',54.49627784,18.51428032,'Małokacka 13, 81-654 Gdynia, Polska'),(29,10,'Shell Gdynia Morska','2019-01-13 00:20:57',1,NULL,'Stacja Paliw, McDonald','oJAY2zLXfE.jpg',54.52685045,18.51257980,'Morska 58, 81-225 Gdynia, Polska'),(30,11,'TOS Gdynia','2019-01-13 00:24:44',1,NULL,'TOS GDYNIA Sp. z o.o. Okręgowa stacja kontroli pojazdów, Przeglądy samochodów osobowych, ciężarowych, taxi.','Fnos2t9Jsr.jpg',54.54115569,18.48596156,'Hutnicza 7B, 81-061 Gdynia, Polska'),(31,12,'Hurtownia Motoryzacyjna GORDON','2019-01-13 00:28:37',1,NULL,'Hurtownia Motoryzacyjna GORDON Hutnicza','xOb1jTCJuz.jpg',54.54412754,18.48109603,'Hutnicza 12A, 81-061 Gdynia, Polska'),(32,10,'Circle K Gdynia Redłowo','2019-01-13 03:57:16',1,NULL,'Circle K Gdynia Extra',NULL,54.49161691,18.54058743,'aleja Zwycięstwa 132, 81-506 Gdynia, Polska'),(33,10,'eMila Karwiny','2019-01-13 03:59:44',1,NULL,'Stacja samoobsługowa.',NULL,54.47276838,18.48059177,'Nowowiczlińska 2, 81-577 Gdynia, Polska'),(34,11,'Auto Fit','2019-01-13 04:02:10',1,NULL,'Auto Fit Gdynia Chwarzno',NULL,54.50171700,18.46337199,'Chwarznieńska 170D, 81-001 Gdynia, Polska'),(35,11,'Europak','2019-01-13 04:04:11',1,NULL,'Stacja Kontroli Pojazdów Europak Gdynia Wielki Kack',NULL,54.46884674,18.50429714,'Wielkopolska 393, 81-514 Gdynia, Polska'),(36,13,'Auto-Plus','2019-01-13 04:07:09',1,NULL,'Auto-Plus Chwaszczyno',NULL,54.44553103,18.42278769,'Wiejska 22, 80-209 Chwaszczyno, Polska'),(37,13,'Auto LPG','2019-01-13 04:08:06',1,NULL,'Auto LPG Sopot',NULL,54.44794863,18.55744332,'Armii Krajowej 122, 81-824 Sopot, Polska'),(38,12,'Best Parts','2019-01-13 04:11:46',1,NULL,'Części do samochodów osobowych',NULL,54.47867048,18.34239534,'Bema 4, 84-208 Kielno, Polska'),(39,15,'Nowy Test Gdańsk','2019-01-13 11:38:01',1,NULL,'Nowy Test','1K6FjUhnKw.jpg',54.42174145,18.57715300,'Gdyńska 5C, 81-861 Gdańsk, Polska'),(40,15,'Gdynia','2019-01-14 09:26:12',1,NULL,'hjghj',NULL,54.41694717,18.58569315,'Leszka Białego 52, 80-353 Gdańsk, Polska');
/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_product_name_place_id_uindex` (`name`,`place_id`),
  KEY `event_calls_events_id_fk` (`place_id`),
  KEY `products_users_id_fk` (`created_by`),
  CONSTRAINT `event_calls_events_id_fk` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`),
  CONSTRAINT `products_users_id_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (39,28,'BP95',495,'2019-01-13 00:18:23',1),(40,28,'Diesel',509,'2019-01-13 00:18:40',1),(41,28,'LPG',241,'2019-01-13 00:18:57',1),(42,29,'Diesel',521,'2019-01-13 00:21:21',1),(43,29,'BP95',515,'2019-01-13 00:21:46',1),(44,29,'LPG',220,'2019-01-13 00:21:59',1),(45,30,'Przegląd Samochodu Osobowego',9900,'2019-01-13 00:25:19',1),(46,30,'Sprawdzenie luzów w zawieszeniu',2500,'2019-01-13 00:26:21',1),(47,31,'Tłumik końcowy Ford Focus',22000,'2019-01-13 00:29:19',1),(48,32,'BP95',517,'2019-01-13 03:57:38',1),(49,32,'Diesel',537,'2019-01-13 03:57:52',1),(50,33,'Diesel',479,'2019-01-13 04:00:02',1),(51,33,'BP95',499,'2019-01-13 04:00:13',1),(52,34,'Przegląd Samochodu Osobowego',12000,'2019-01-13 04:02:22',1),(53,34,'Sprawdzenie luzów w zawieszeniu',3000,'2019-01-13 04:02:35',1),(54,35,'Przegląd Samochodu Osobowego',10000,'2019-01-13 04:04:29',1),(55,35,'Sprawdzenie luzów w zawieszeniu',1500,'2019-01-13 04:04:41',1),(56,36,'Wymiana Klocków Hamulcowych',10000,'2019-01-13 04:08:54',1),(57,36,'Wymiana oleju',5000,'2019-01-13 04:09:08',1),(58,37,'Wymiana oleju',7000,'2019-01-13 04:09:36',1),(60,38,'Tłumik końcowy Ford Focus',17000,'2019-01-13 04:12:03',1),(61,39,'LPG',150,'2019-01-13 11:38:31',1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_users_id_fk` (`created_by`),
  CONSTRAINT `users_users_id_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'tomasz@toperko.pl','$2y$10$SGs5fBkXzeR.UMDo4Hj5ruAafNya3VBINhRMtTDpHqfXQZVQdPMxq',100,NULL,'2018-12-08 13:10:09','2019-01-14 09:25:39'),(4,'nowy2@User.pl','$2y$10$jD96kDTbiXPajYe4xmuRCuxDclRNjkpulycAQzM9kA2FV9RgUZxH2',10,1,'2019-01-10 20:16:21','2019-01-10 20:17:06'),(5,'tomasz@test.pl','$2y$10$ng5f9rF5KTtSc.8RRfT2mO.bcnA31ny.03IehZyLIM63.NXE0Ic.a',10,1,'2019-01-13 05:29:24',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-16 12:31:44
